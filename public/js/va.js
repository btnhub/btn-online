//REF: https://tokbox.com/developer/guides/publish-stream/js/
//REF: https://tokbox.com/developer/guides/subscribe-stream/js/

var publisher = session = null;
var publisher_video = publisher_microphone = false;
var connected = false,
  publisherInitialized = false;

//initializeSession();

function showMessage(message){
	var alertDiv = document.getElementById("video-alert-msg");
	alertDiv.innerHTML = message;
	document.getElementById('video-alert').style.visibility = 'visible';
}

function closeVideoAlert(){
	document.getElementById('video-alert').style.visibility = 'hidden';
}

function toggleVideo(role, video_id){
	var video_symbol;

	if (role == 'publisher') {
		publisher_video = !publisher_video;
		publisher.publishVideo(publisher_video);

		if (publisher_video) {
			video_symbol = '<i class="fas fa-video" aria-hidden="true"></i>';
		}
		else{
			video_symbol = '<i class="fa fa-video-slash" aria-hidden="true"></i>';
		}	
	}
	
	document.getElementById(video_id).innerHTML = video_symbol;
	console.log("Publisher Video State: " + publisher_video);
}

// Handling all of our errors here by alerting them
function handleError(error) {
if (error) {
	alert(error.message);
	}
}

function initializeSession() {
	session = OT.initSession(apiKey, sessionId);
	var publish = function() {
	  if (connected && publisherInitialized) {
	    
	    document.getElementById('publish-button').style.display = "none";
	    
	    session.publish(publisher, function(err) {
				  if (err) {
				    switch (err.name) {
				      case "OT_NOT_CONNECTED":
				        showMessage("Publishing your video failed. You are not connected to the internet.");
				        break;
				      case "OT_CREATE_PEER_CONNECTION_FAILED":
				        showMessage("Publishing your video failed. This could be due to a restrictive firewall.");
				        break;
				      default:
				        showMessage("An unknown error occurred while trying to publish your video. Please try again later.");
				    }
				    publisher.destroy();
				    publisher = null;
				  }
				});
	  }
	};
	
	session.on({
		streamCreated: function(event) {
			var subscriberProperties = {insertMode: 'append',
										/*fitMode: "contain",*/
										width: '100%',
										height: '100%'};

			session.subscribe(event.stream, 'subscriber', subscriberProperties, function (err) {
					if (err) {
						showMessage('Streaming connection failed. This could be due to a restrictive firewall.');
					}
				});
		},
		streamDestroyed: function (event) {
  			//when stream other than your own leaves session
  			
  			console.log("Stream stopped. Reason: " + event.reason);
  			//event.preventDefault(); //the corresponding Subscriber objects (there could be more than one) are destroyed and removed from the HTML DOM.
  			//var subscribers = session.getSubscribersForStream(event.stream);
  			// Now you can adjust the DOM elements around each subscriber to the stream, and then delete it yourself.
  			//  You can then delete the Subscriber object (and its DOM element) by calling the destroy() method of the Subscriber object.
			if (event.reason === 'networkDisconnected') {
				event.preventDefault();
				var subscribers = session.getSubscribersForStream(event.stream);
				
				if (subscribers.length > 0) {
					var subscriber = document.getElementById(subscribers[0].id);
					// Display error message inside the Subscriber
					subscriber.innerHTML = 'Lost connection. This could be due to your internet connection or because the other party lost their connection.';
					event.preventDefault();   // Prevent the Subscriber from being removed
				}
			}
		},
	});

	/*subscriber.on({
		disconnected: function() {
			//Client drops connection (e.g. drop in network connectivity)
			// Display a user interface notification.
			},
		connected: function() {
			//Connectivity restored
			// Adjust user interface.
		},
		destroyed: function() {
			//Cannot restore connection
			// Adjust user interface.
		},
		videoDisabled: function(event) {
			//When the subscriber's video is disabled
			// You may want to hide the subscriber video element:
			domElement = document.getElementById(subscriber.id);
			domElement.style["visibility"] = "hidden";

			// You may want to add or adjust other UI.
		},
		videoEnabled: function(event) {
			//when video resumes
			// You may want to display the subscriber video element,
			// if it was hidden:
			domElement = document.getElementById(subscriber.id);
			domElement.style["visibility"] = "visible";

			// You may want to add or adjust other UI.
		},
		videoDimensionsChanged: function(event) {
			//If Mobile screen resizes or screen-shared window resizes
			//subscriber.element.style.width = event.newValue.width + 'px';
			//subscriber.element.style.height = event.newValue.height + 'px';
			// You may want to adjust other UI.
		}
	
	});*/

	// Create a publisher
	if (publisher == null) {

	}
	publisher = OT.initPublisher('publisher', {
						insertMode: 'append',
						/*fitMode: "contain",*/
						name:local_user.username,
						width: '100%',
						height: '100%'
						/*resolution: '1280x720'*/ /*can also be '640x480' & '320x240', default is 640x480*/
						/*frameRate: 7*/ /*can also be 30, 15, 7, and 1*/
					}, function(error) {
						  if (error) {
						    if (error.name === 'OT_USER_MEDIA_ACCESS_DENIED') {
						      // Access denied can also be handled by the accessDenied event
						      showMessage('Please allow access to the Camera and Microphone and try publishing again.');
						    } else {
						      showMessage('Failed to get access to your camera or microphone. Please check that your webcam is connected and not being used by another application and try again.');
						    }

						    publisher.destroy();
						    publisher = null;
						  } else {
						    publisherInitialized = true;
						    publish();
						  }
					});

	//Detect whether the user has granted access to the camera & microphone
	publisher.on({
	  	accessDialogOpened: function (event) {
	    	// Show allow camera message
	    	//alert("please allow camera");
	    	//pleaseAllowCamera.style.display = 'block';
	  	},
	  	accessDialogClosed: function (event) {
	    	// Hide allow camera message
	    	//pleaseAllowCamera.style.display = 'none';
	  	},
	  	accessAllowed: function (event) {
	    	// The user has granted access to the camera and mic.
	  	},
	  	accessDenied: function accessDeniedHandler(event) {
	    	// The user has denied access to the camera and mic.
	    	showMessage('Please allow access to the Camera and Microphone.');
	  	},
	  	streamCreated: function (event) {
    		//console.log("Publisher started streaming.");
  		},
	  	streamDestroyed: function (event) {
		  	//event.preventDefault(); //to prevent Publisher from being destroyed and removed from HTML DOM
		  	if (event.reason === 'networkDisconnected') {
		    	showMessage('Your publisher lost its connection. Please check your internet connection and try publishing again.');
	    	}

		  showMessage("The publisher stopped streaming. Reason: "
		    + event.reason);
	  	}
	});

	// Connect to the session
	session.connect(token, function(error) {
		// If the connection is successful, initialize a publisher and publish to the session
		if (error) {
			showMessage(error.message);
		} else {
			connected = true;
    		publish();
		}
	});
}

function stopPublishing(){
	 //Possible ways to disconnect user
	 session.unpublish(publisher);
	 publisher.destroy(); // delete Publisher object & remove from HTML DOM
}

function stopSubscribing(){
	session.unsubscribe(stream); //The Subscriber object is destroyed, and the stream display is removed from the HTML DOM.
}

