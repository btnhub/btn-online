function submitMessage(){
	var msg = document.getElementById('chat-input').value;

	appendMessage("Me!", msg);

	broadcastMessage(msg);
}

function receiveMessage(new_message){
	var username = new_message.username;
	var msg = new_message.msg;

	appendMessage(username, msg);
}

function appendMessage(sender, message)){
	var chat_div = document.getElementById('chat');
	var username_div = document.createElement('div');
	username_div.className = "receiver username";
	username_div.innerHTML = sender;
	chat_div.appendChild(username_div);

	var msg_div = document.createElement('div');
	msg_div.className = "receiver msg";
	msg_div.innerHTML = message;
	chat_div.appendChild(msg_div);

	return true;
}