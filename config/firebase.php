<?php

return [
	'json' => env('FIREBASE_JSON', null),
	'url' => env('FIREBASE_URL', null),
];
