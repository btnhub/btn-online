function Formula(formulaLaTex, leftX, leftY, font, fontSize, fillColor, winWidth){
	this.type = "formula";
	this.dragging = false;
  this.selected = false;
	this.formulaLaTex = formulaLaTex;
	this.leftX = leftX;
	this.leftY = leftY;
  this.rightX = null;
  this.rightY = null;
	this.font = font;
	this.fontSize = fontSize;
	this.fillColor = fillColor;
  this.winWidth = winWidth;
	this.active = true;

	this.textBoxId = Math.random().toString(36).substring(3).toUpperCase();

  this.left = null;
  this.top = null;
  this.width = null;
  this.height = null;

  this.image = null;
  this.src_base64 = null;
}

Formula.prototype.draw = function() {
  var scale_ratio = width / this.winWidth;

  this.left = this.leftX;
  this.top = this.leftY;

  if (this.image) {
    push();
    scale(scale_ratio);
    image(this.image, this.left, this.top);

    if (this.selected) {
      leftX = this.leftX;
      topY = this.leftY;
      rightX = leftX + this.width;
      bottomY = topY + this.height;
      
      //Create rectangle around ellipse
      push();
      var padding = 5;
      stroke(selectShapeColor);
      strokeWeight(2);
      noFill();
      //Outline
      rectMode(CORNERS);
      rect(leftX, topY, rightX, bottomY);
      //corners
      rectMode(CENTER);
      fill(selectShapeColor)
      rect(leftX, topY, padding * 4, padding * 4);
      //rect(rightX, topY, padding, padding);
      //rect(leftX, bottomY, padding, padding);
      rect(rightX, bottomY, padding * 4, padding * 4);  
      pop();
    }
    pop();
  }

	/*var thisFormulaSpan = document.getElementById(this.textBoxId);

  if (thisFormulaSpan) {
    //scale based on screen size
    thisFormulaSpan.style.left = parseInt(this.leftX) * scale_ratio + "px";
    thisFormulaSpan.style.top = parseInt(this.leftY) * scale_ratio + "px";
    // Code for Safari
    thisFormulaSpan.style.WebkitTransform = "scale(" + scale_ratio + ")";
    thisFormulaSpan.style.WebkitTransform = "translate(" + transX + "px," + transY + "px)";
    // Code for IE9
    thisFormulaSpan.style.msTransform = "scale(" + scale_ratio  + ")";
    thisFormulaSpan.style.msTransform = "translate(" + transX + "px," + transY + "px)";
    // Standard syntax
    thisFormulaSpan.style.transform = "scale(" + scale_ratio  + ")";
    thisFormulaSpan.style.transform = "translate(" + transX + "px," + transY + "px)";
    
    //update Formula 
    //1) replace span with just latex string 
    //2) conver with MathQuill
    // thisFormulaSpan.innerHTML = this.formulaLaTex;
    // var answerMathField = MQ.MathField(thisFormulaSpan, {
    //   handlers: {
    //     edit: function() {
    //       var enteredMath = answerMathField.latex(); // Get entered math in LaTeX format
    //     }
    //   }
    // });
  }
	else if (!thisFormulaSpan && this.formulaLaTex) {
		thisFormulaSpan = this.createFormulaBox(this.leftX, this.leftY);
	}*/

  //scale element
  	
};


Formula.prototype.createFormulaBox = function(winPosX, winPosY) {
	 var active_focus = false;

   this.leftX = winPosX;
	 this.leftY = winPosY;
   
   activeFormulaId = this.textBoxId;
   formula_font_size = this.fontSize;

  	formulaSpan = createElement('span');
  	formulaSpan.id(this.textBoxId);
    formulaSpan.parent("canvas-region"); //may need to change to active_board_id
  	formulaSpan.position(this.leftX, this.leftY);
  	formulaSpan.html(this.formulaLaTex);
  	formulaSpan.style('font-size', this.fontSize + "px");
  	formulaSpan.style('color', this.fillColor);
  	formulaSpan.style('border', "none");

    formulaMove = createElement('span');
    formulaMove.id(this.textBoxId + "-move");
    formulaMove.parent("canvas-region");
    formulaMove.position(parseInt(this.leftX - 2), parseInt(this.leftY - 27));
    formulaMove.style('height', "25px");    
    formulaMove.style('width', "25px");        
    formulaMove.style('background-color', selectShapeColor);    
    formulaMove.style('cursor', "crosshair");    
    formulaMove.mousePressed(function (){
                    console.log('Ready to move!');
                });

  	if (this.formulaLaTex == '') {
  		formulaSpan.html("x=...");
  	}
  
    var formula = document.getElementById(this.textBoxId);
  	
  	var answerMathField = MQ.MathField(formula, {
	    handlers: {
	      edit: function() {
	        var enteredMath = answerMathField.latex(); // Get entered math in LaTeX format
	      }
	    }
  	});

  	formulaSpan.elt.addEventListener("focusin", function(){
    				//show formula menu
            var formula_menu = document.getElementById('formula-options');
            formula_menu.style.left = this.style.left;
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
            formula_menu.style.display = "block";
            setActiveFormulaId(this.id);            
    			});

  	formulaSpan.elt.addEventListener("keyup", function(){
    				updateFormulaText(answerMathField.latex());
            
            //update location of formula menu
            var formula_menu = document.getElementById('formula-options');
            //formula_menu.style.left = this.style.left;
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
    			});

    formulaSpan.elt.addEventListener("focusout", function(){
    				updateFormulaText(answerMathField.latex());
            resetActiveFormulaId();

    				//document.getElementById('formula-options').style.display = "none";
    			});
   	

    answerMathField.focus();  
   	
   	activeMathField = answerMathField;

    this.rightX = this.leftX + formulaSpan.elt.offsetWidth;
    this.rightY = this.leftY + formulaSpan.elt.offsetHeight;
    
    this.width = formulaSpan.elt.offsetWidth;
    this.height = formulaSpan.elt.offsetHeight;
};

Formula.prototype.locate = function(clickX, clickY) {
  /*if(clickX >= this.leftX && clickX <= this.rightX && clickY >= this.leftY && clickY <= this.rightY){
      this.selected = true;
      return true;
  }
  else{
    this.selected = false;
    return false;
  }*/

  var padding = 5;
  //Find Formula based on click location
  var scale_ratio = width / this.winWidth;

  scaled_left = this.left * scale_ratio;
  scaled_top = this.top * scale_ratio;
  scaled_width = this.width * scale_ratio;
  scaled_height = this.height * scale_ratio;

  if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
    this.selected = true;

      //Find NW corner
      if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
        mode = "resize";
        console.log("NW");
        //originalCornerX = this.leftX;
        //originalCornerY = this.leftY;
        cornerUpdated = "NW";
      }
      //Find SE corner
      if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
        mode = "resize";
        console.log("SE");
        //originalCornerX = this.rightX;
        //originalCornerY = this.rightY;
        cornerUpdated = "SE";
      }
      return true;
  }
  else{
      this.selected = false;
      return false;
  }
};

Formula.prototype.move = function(dx, dy) {
  this.leftX += dx;
  this.leftY += dy;
  //this.rightX += dx;
  //this.rightY += dy;

  var thisFormulaSpan = document.getElementById(this.textBoxId);
  thisFormulaSpan.style.left = this.leftX + "px";
  thisFormulaSpan.style.top = this.leftY + "px";
};

Formula.prototype.delete = function(clickX, clickY) {
	document.getElementById(this.textBoxId).remove();
};

Formula.prototype.createFormulaImage = function() {
  var cog_url = "https://latex.codecogs.com/svg.latex?";
  temp_fontSize = "\\Huge ";
  temp_color = "\\color{" + "Purple" + "}";

  var formula = temp_fontSize + "{" + temp_color + this.formulaLaTex + "}";
  var cog = cog_url + encodeURIComponent(formula);
  var img = createImg(cog);
  img.hide();
  this.image = img;
}