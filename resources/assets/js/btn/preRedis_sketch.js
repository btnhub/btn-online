var canvas, shape_option, fs, mode, bgcolor, shapeFillColor, textColor, freeFormColor, selectShapeColor, resizeShapeColor, grid;  

//For Adding Shapes To Canvas
var shapes, undone_shapes;
var drag_canvas, new_shape_index, selected_shape, selected_shape_index, startX, startY, originalLeftX, originalLeftY, originalRightX, originalRightY, originalCornerX, originalCornerY, cornerUpdated;

//var newTextArea, resize_corner;
var new_text, new_pencil;
var fileButton, fullScreenButton;

var formulaSpan, activeFormulaId, activeMathField;

var zoom, zMin, zMax, zSensitivity, transX, transY;

var shapes_archive, current_shapes_index, undo_steps, redo_steps;

function setup() {
  //call array that stored all the previous events/drawings/messaegs and create whiteboard with those events
  /*var rooms, room, roles, role;
  var params = getURLParams();
  if (params.room) {
    room = params.room;
  }
  else{
    rooms = ['Math', 'Physics'];
    room = rooms[Math.floor(Math.random()*rooms.length)];
  }

  if (params.role) {
    role = params.role;
  }
  else{
    roles = ['tutor', 'student', 'free_trial'];
    role = roles[Math.floor(Math.random()*roles.length)];
  }*/

  //joinRoom(room);
  initializeBoard();
  createToolbar();  
  sizeSlider = select("#penWeight");
  lineWeightSlider = select("#lineWeight");
  fontSize = select("#fontSize");
  //createTab(); //create tab and canvas
  canvas = createCanvas(windowWidth, windowHeight);
  var canvas_parent = select("#canvas-region");
  canvas.parent(canvas_parent);
  //Canvas only events
  canvas.mouseClicked(function(){
    // if (newTextArea) {
    //   newTextArea.remove();  
    // }
  });

  canvas.mousePressed(function(){
    for (var shape of shapes){
      shape.selected = false;
    }

    // if (shape_option){
    //   originalX = mouseX - transX;
    //   originalY = mouseY - transY;  
    // }

    startX = mouseX - transX;
    startY = mouseY - transY;
    
    switch (mode){
      case "drag":
        cursor(HAND);
        findShape(); //shapes.js
        break;

      case "draw":
        new_shape_index = shapes.length;
        
        if (shape_option == "pencil") {
          new_pencil = new Pencil(sizeSlider.value(), freeFormColor, width);    
        }
        if (shape_option == "eraser") {
          shape_option = "pencil";
          new_pencil = new Pencil(sizeSlider.value(), "white", width);    
        }

        if (shape_option == "highlighter") {
          //shape_option = 'pencil';
          var highColorRGB = color(freeFormColor);
          var highColor = highColorRGB.toString().replace("1)", "0.3)");
          new_pencil = new Pencil(sizeSlider.value(), highColor, width);    
          new_pencil.highlighter = true;
        }
        break;
    }
  });

  canvas.mouseReleased(function(){
    if (mode == "drag"){
      cursor(HAND);
      
      if (selected_shape){
        selected_shape.drag = false;
        selected_shape = selected_shape_index = null;  
        startX = startY = null;
        originalLeftX = originalLeftY = originalRightX = originalRightY = null;
      }
    }

    if (mode =="draw") {
      switch (shape_option){
        case "text":
          cursor(TEXT);
          new_text = new Write('', mouseX - transX, mouseY - transY, "Sans", Number(fontSize.elt[fontSize.elt.selectedIndex].text), textColor, width);
          new_text.createTextBox(mouseX - transX, mouseY - transY);
          break;

        case "formula":
          cursor(TEXT);
          new_formula = new Formula('', mouseX - transX, mouseY - transY, "Sans", fontSize.elt[fontSize.elt.selectedIndex].text, textColor, width);
          shapes[new_shape_index] = new_formula;
          new_formula.createFormulaBox(winMouseX - transX, winMouseY - transY);
          break;
      }
      
      //For Redis
      broadcastShape(JSON.stringify(shapes), "update");
    }
    
    if (mode == "resize"){
      mode = "drag";
      cursor(HAND);
      originalCornerX = originalCornerY = cornerUpdated = null;
    }

    //shapes_archive.push(shapes);
    current_shapes_index++;
    undo_steps++;
    redo_steps = 0;
    shapes_archive[current_shapes_index] = JSON.stringify(shapes);
  });
}

function initializeBoard(){
  shape_option = "pencil";
  mode = "draw";
  fs = false; //full screen
  canvas = null;
  drag_canvas = false;
  //var eraser = null;
  bgcolor = '#E0E0E0';
  //var fillColor = "#2a9bbe";
  shapeFillColor = "white";
  textColor = "black";
  freeFormColor = "black";

  selectShapeColor = "#964FE8"; //when shapes are located and selected
  resizeShapeColor = "#660066";
  grid = true; 

  shapes = undone_shapes = [];
  new_text = new_pencil = null;
  new_shape_index = selected_shape = selected_shape_index = startX = startY = originalLeftX = originalLeftY = originalRightX = originalRightY = originalCornerX = originalCornerY = cornerUpdated = null;

  formulaSpan = activeFormulaId = null; 
  activeMathField = null;

  zoom = 1.00;
  zMin = 0.8;
  zMax = 5.00;
  zSensitivity = 0.005;

  transX = transY = 0;

  shapes_archive = [];
  current_shapes_index = undo_steps = redo_steps = 0;
}

function resetSketch(){
  var decision = confirm('Are you sure you want to erase the board? Consider saving the board first by clicking the  download arrow button');
  if (decision) {
    initializeBoard();
  }
}

function draw() {
  background(255);
  
  scale(zoom);

  if (grid) {
    stroke(230);
    //Horizontal lines
    for (var i = 0; i <= height / zoom; i+=10) {
      
      if (i % 50 == 0){
        strokeWeight(2);
        
      }
      else{
        strokeWeight(1);
      }
      line(0, i, width / zoom, i);
    }
    
    //Vertical lines
    for (var i = 0; i <= width / zoom; i+=10) {
      if (i % 50 == 0){
        strokeWeight(2);
        
      }
      else{
        strokeWeight(1);
      }
      line(i, 0, i, height / zoom);
    }
  }
  
  //draw own shapes
  translate(transX, transY);

  for (var shape of shapes){
    shape.draw();
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  //Add code to reposition & resize shapes
}

function mouseDragged() {
  if (mode =="drag" || (mode =="draw" && shape_option !="text")) {
    updateShapes();  
  }

  if(mode == "resize"){
    for(var shape of shapes){
      if (shape.selected == true) {
        cursor(MOVE);
        shape.resize();
        broadcastShape(JSON.stringify(shapes));
        break;
      }
    }
  }
  return false;
}

function mouseWheel(event){

  //scale to zoom in and out
  /*zoom += zSensitivity * event.delta;
  zoom = constrain(zoom, zMin, zMax);
  //uncomment to block page scrolling
  return false;*/
}

function keyPressed(){
  //Updating Selected Shape
  for (var shape of shapes){
    if (shape.selected) {
      switch (keyCode){
        case UP_ARROW:
          shape.move(0,-5);
          broadcastShape(JSON.stringify(shapes));
          return false;
          break;
        case DOWN_ARROW:
          shape.move(0,5);
          broadcastShape(JSON.stringify(shapes));
          return false;
          break;
        case LEFT_ARROW:
          shape.move(-5,0);
          broadcastShape(JSON.stringify(shapes));
          return false;
          break;
        case RIGHT_ARROW:
          shape.move(5,0);
          broadcastShape(JSON.stringify(shapes));
          return false;
          break;
        case DELETE:
          deleteShape();
          break;
      }
    }
  }
  //shortcuts
  switch (keyCode){
    case ESCAPE:
      mode = "drag";
      cursor(HAND);

      for (var shape of shapes){
        shape.selected = false;
      }
      break; 
  }

  //Multipe Key Shortcuts
  if(keyIsDown(CONTROL)){
    switch  (key){
      case "Z":
        undo();
        return false; //prevent default browser behavior
        break;
      case "Y":
        redo();
        return false; //prevent default browser behavior
        break;
      //Copy selected shape
      //Paste selected shape
      //Clear Canvas
      //New Canvas
      //Delete selection
      //Save One Board, save all boards
      //Select All
    }
  }
}

function changeMode(selected_mode){
  mode = selected_mode;
}

function selectColor(selected_color, shape_type){
  switch (shape_type){
    case "freeform":
      freeFormColor = selected_color;
      break;

    case "shape":
      shapeFillColor = selected_color;
      break;

    case "text":
      textColor = selected_color;
      break;
  }

  for(var shape of shapes){
    if (shape.selected == true) {
      shape.fillColor = selected_color;
      shape.strokeColor = selected_color;
      broadcastShape(JSON.stringify(shapes), "update");
    }
  }
}

function selectLineWeight(lineWeight){
  for(var shape of shapes){
    if (shape.selected == true && shape.lineWeight) {
      shape.lineWeight = lineWeight;
      broadcastShape(JSON.stringify(shapes), "update");
    }
  }
}

function undo(){
  if (undo_steps > 0) {
    current_shapes_index--;
    undo_steps--;
    redo_steps++;
    
    if (current_shapes_index > 0) {
      receiveShapes(JSON.parse(shapes_archive[current_shapes_index]));  
    }
    else if(current_shapes_index == 0){
      shapes = [];
    }
  }

  /*if (shapes.length) {
    undone_shapes.push(shapes.pop());
    //UPDATE: account for other changes like moving an object
  }*/
}

function redo(){
  if (redo_steps > 0) {
    current_shapes_index++;
    undo_steps++;
    redo_steps--;

    if (current_shapes_index > 0) {
      receiveShapes(JSON.parse(shapes_archive[current_shapes_index]));  
    }
    else if(current_shapes_index == 0){
      shapes = [];
    }
  }

  /*if (undone_shapes.length) {
    shapes.push(undone_shapes.pop());  
  }*/
}

function showShapeOptions(shape_option_id){
  var menus = document.getElementsByClassName("shapes-menu");
  for(var i = 0; i < menus.length; i++)
  {
     menus[i].style.display = 'none';
  }
  document.getElementById(shape_option_id).style.display = "inline";
}

function uploadFile(file){
  if (file.type == 'image') {
    var count = 0;
    var img = createImg(file.data);
    var img_width = img.width;
    var img_height = img.height;
    img.hide();

    if (img_width > width) {
      //scale image to canvas
      var new_width = map(img_width, 0, img_width, 0, width);
      var new_height = map(img_height, 0, img_height, 0, height);
      img.size(new_width, new_height);
    }

    new_image = new Image(img, 10, 10, img_width, img_height, width);
    new_image.src_base64 = img.elt.src;

    shapes[shapes.length] = new_image;
    
    mode = "drag";
    broadcastShape(JSON.stringify(shapes), 'update');
  }
}

function saveBoard(){
  var imgCanvasWidthMax = 810; //2550px = 300dpi
  var imgCanvasHeightMax = 1048; //3300px = 300dpi
  var pageRatio = imgCanvasWidthMax / imgCanvasHeightMax;
  var pagePadding = 48; //150px = 0.5"
  var shapeMinX = shapeMinY = shapeMaxX = shapeMaxY = 0;
  var dx, dy, drawingWidth, drawingHeight;
  var original_zoom = zoom;
  var formulaSpanIds = [];

  for(var shape of shapes){
    shape.selected = false;

    shapeMinX = Math.min(shapeMinX, shape.leftX, shape.rightX);
    shapeMinY = Math.min(shapeMinY, shape.leftY, shape.rightY);
    shapeMaxX = Math.max(shapeMaxX, shape.leftX, shape.rightX);
    shapeMaxY = Math.max(shapeMaxY, shape.leftY, shape.rightY);
    
    //replace formula span with image?
  }
  
  //calculate delta to move minX and minY to padding coordinate
  dx = pagePadding - shapeMinX;
  dy = pagePadding - shapeMinY;
  drawingWidth = shapeMaxX - shapeMinX + (pagePadding * 2);
  drawingHeight = shapeMaxY - shapeMinY + (pagePadding * 2);

  resizeCanvas(imgCanvasWidthMax - (pagePadding * 2), imgCanvasHeightMax - (pagePadding * 2));
  html2canvas(document.getElementById('canvas-region'), {
      onrendered: function(canvas_snapshot){
        var temp_img = canvas_snapshot.toDataURL('image/png');
        var doc = new jsPDF("portrait", "in", "letter");
        //jsPDF(orientation, unit, format, compress);
        doc.addImage(temp_img, 'JPEG', 0.5, 0.5);
        doc.save('test_board.pdf');
      }
  });
  
  //move all shapes
  for(var shape of shapes){
    //shape.move(dx, dy);
    var resize_ratio = drawingWidth / shape.winWidth;
    push();
      resizeCanvas(drawingWidth, drawingHeight);
      scale(resize_ratio);
      shape.move(dx, dy);
      shape.draw();
    pop();
  }

  zoom = 1;
  //resizeCanvas(drawingWidth, drawingHeight);
  //redraw();

  push();
  fill(0);
  noStroke();
  textSize(12);
  textFont('Georgia');
  text("Courtesy of BuffTutor Online", 10, 10);
  pop();
  //save(canvas, 'whiteboard.jpg');
  
  html2canvas(document.getElementById('canvas-region'), {
      onrendered: function(canvas_snapshot){
        var temp_img = canvas_snapshot.toDataURL('image/png');
        var doc = new jsPDF("portrait", "in", "letter");
        //jsPDF(orientation, unit, format, compress);
        doc.addImage(temp_img, 'JPEG', 0, 0);
        doc.save('test_board-shifted.pdf');
      }
  });

  /*var node = document.getElementById(temp_formula_id);

    domtoimage.toPng(node).then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
    }).catch(function (error) {
        console.error('oops, something went wrong!', error);
    });*/

  //move all shapes back
  for(var shape of shapes){
    shape.move(-dx, -dy);
  }

  resizeCanvas(windowWidth, windowHeight);
  zoom = original_zoom;
  redraw();
}