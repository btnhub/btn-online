var baseUrl = window.location.protocol + "//" + window.location.host + "/";

function toggleChat(){
	var chat_window = document.getElementById('chat-window');

	if (chat_window.style.display === "none") {
        chat_window.style.display = "block";
        var chat_image = "<img src='" + baseUrl + "images/symbols/chat.svg'>";
        document.getElementById('chat-button').innerHTML = chat_image;
		
    } else {
        chat_window.style.display = "none";
    }
}

function updateRoom(data){
	var user = data.user;
	var action = data.action;
	var user_id = data.id;

	var chat_div = document.getElementById('chat');
	var announce_div = document.createElement('div');

	switch (action){
		case "joined_room":
			announce_div.className = "announcement";
			announce_div.innerHTML = user + " joined the room.";
			chat_div.appendChild(announce_div);
			break;
		
		case "left_room":
			announce_div.className = "announcement";
			announce_div.innerHTML = user + " left the room.";
			chat_div.appendChild(announce_div);
			break;
	}

	chat_div.scrollTop = chat_div.scrollHeight;
}

function sendChatMessage(event){
	if (event.keyCode === 13) {
	    submitMessage();
	}
	event.preventDefault();
}

function loadPriorMessages(messages){
	for (var message of messages){
		appendMessage(message.username, message.msg);
	}
	
	var chat_image = "<img src='" + baseUrl + "images/symbols/chat-new-message.svg'>";
	document.getElementById('chat-button').innerHTML = chat_image;
}

function submitMessage(){
	var chat_input = document.getElementById('chat-input');
	var msg = chat_input.value;

	appendMessage(local_user.username, msg);
	chat_input.value = "";

	broadcastMessage(msg);

	return true;
}

function receiveMessage(new_message){
	var username = new_message.username;
	var msg = new_message.msg;

	appendMessage(username, msg);

	var chat_window = document.getElementById('chat-window');
	if (chat_window.style.display == "none" || chat_window.style.visibility == "hidden") {
		var chat_image = "<img src='" + baseUrl + "images/symbols/chat-new-message.svg'>";
		document.getElementById('chat-button').innerHTML = chat_image;
	}
	
}

function appendMessage(username, message){
	role = (local_user.username == username) ? "sender" : "receiver";

	var chat_div = document.getElementById('chat');
	var username_div = document.createElement('div');
	username_div.className = role + " username";
	username_div.innerHTML = username;
	
	//get sender of last message
	var senders = document.getElementsByClassName("username");
	var last_sender = senders.length ? senders[senders.length - 1].innerHTML : null;	
	
	if (senders.length == 0 || last_sender != username) {
		chat_div.appendChild(username_div);			
	}
	
	var msg_div = document.createElement('div');
	msg_div.className = role + " msg";
	msg_div.innerHTML = message;
	chat_div.appendChild(msg_div);

	chat_div.scrollTop = chat_div.scrollHeight;

	return true;
}