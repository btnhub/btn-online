function selectShape(chosen_shape){
	mode = "draw";
	cursor(CROSS);

	shape_option = chosen_shape;
	
	if (shape_option == 'text') {
		cursor(TEXT);
	}
}

function dragCanvas(){
	var dx = mouseX - pmouseX;
	var dy = mouseY - pmouseY;

	cursor(MOVE);	
	transX += dx;
	transY += dy;
	//redraw();
}

function receiveShapes(data){
	//turn data into objects
	var received_shapes = [];

	for (var i = 0; i < data.length; i++) {
		switch (data[i].type){
			case "line":
				received_shapes[i] = new Line(data[i].startX, data[i].startY, data[i].endX, data[i].endY, data[i].strokeColor, data[i].lineWeight, false, data[i].winWidth);
				break;

			case "arrow":
				received_shapes[i] = new Line(data[i].startX, data[i].startY, data[i].endX, data[i].endY, data[i].strokeColor, data[i].lineWeight, true, data[i].winWidth);
				received_shapes[i].type = "arrow";
				break;

			case "rect":
				received_shapes[i] = new Rectangle(data[i].leftX, data[i].leftY, data[i].rightX, data[i].rightY, data[i].fillColor, data[i].lineWeight, data[i].winWidth);
				break;

			case "ellipse":
				received_shapes[i] = new Ellipse(data[i].leftX, data[i].leftY, data[i].rightX, data[i].rightY, data[i].fillColor, data[i].strokeColor, data[i].lineWeight, data[i].winWidth);
				break;

			case "pencil":
				received_shapes[i] = new Pencil(data[i].radius, data[i].fillColor, data[i].winWidth);
				received_shapes[i].coordinates = data[i].coordinates;
				break;

			case "text":
				received_shapes[i] = new Write(data[i].textString, data[i].leftX, data[i].leftY, data[i].font, data[i].fontSize, data[i].fillColor, data[i].winWidth);
				received_shapes[i].formattedString = data[i].formattedString;
				break;

			case "formula":
				received_shapes[i] = new Formula(data[i].formulaLaTex, data[i].leftX, data[i].leftY, data[i].font, data[i].fontSize, data[i].fillColor, data[i].winWidth);
				received_shapes[i].textBoxId = data[i].textBoxId;
				
				if (data[i].src_base64) {
					var img = createImg(data[i].src_base64);
					img.hide();
					received_shapes[i].image = img;	
				}
				
				received_shapes[i].url = data[i].url;
			  	received_shapes[i].src_base64 = data[i].src_base64;
			  	received_shapes[i].fontLaTex = data[i].fontLaTex;
			  	received_shapes[i].fontSizeLaTex = data[i].fontSizeLaTex;
				break;

			case "image":
				var img = createImg(data[i].src_base64);
				img.hide();
				received_shapes[i] = new Image(img, data[i].leftX, data[i].leftY, data[i].width, data[i].height, data[i].winWidth);
    			received_shapes[i].src_base64 = img.elt.src;
				break;
		}
	}

	shapes = received_shapes;
	redraw();
}

function findShape(){	    
  deselectAll();

  //Locate Shape, Iterate in reverse
  for (var i = shapes.length - 1; i >= 0; i--) {
  	var shape = shapes[i];
  	if (shape.locate(mouseX - transX, mouseY - transY)) {
  		shape.selected = true;
	    selected_shape = shape;
	    originalLeftX = selected_shape.leftX;
	    originalLeftY = selected_shape.leftY;
	    originalRightX = selected_shape.rightX;
	    originalRightY = selected_shape.rightY;
	    selected_shape.dragging = true;
	    //drag_canvas = false;
	    redraw();
	    break;	
  	}
  	
  }
}

function updateText(){
  new_text.textString = this.value();
  new_text.fitToBox(this.width);

  shapes[new_shape_index] = new_text;
  broadcastShape(JSON.stringify(shapes));
  redraw();
}

function removeTextBox(textBoxId){
	var textBox = document.getElementById(textBoxId);
	
	for (var shape of shapes){
		if (shape.type == "text" && shape.textBoxId == textBoxId) {
			if (textBox.value && textBox.value.trim() != '') {
				shape.textString = textBox.value;
				shape.fitToBox(shape.textBoxWidth);
				shape.show = true;
				shape.draw();
			}
			else{
				//delete empty Text from shapes
				var temp_index = shapes.indexOf(shape);
				if (temp_index != -1) {
					shapes.splice(temp_index, 1);
					broadcastShape(JSON.stringify(shapes));
				}
			}
			break;
	    }
	}
	textBox.remove();
	redraw();
}

function setActiveFormulaId(formulaSpanId){
	activeFormulaId = formulaSpanId;
}

function resetActiveFormulaId(textBoxId){
	var formulaBox = document.getElementById(textBoxId);
	activeFormulaId = null;
	formulaBox.remove();
	for (var shape of shapes){
		if (shape.type == "formula" && shape.textBoxId == textBoxId && shape.formulaLaTex.trim() == '') {
			//delete empty Formula from shapes
			var temp_index = shapes.indexOf(shape);
			if (temp_index != -1) {
				shapes.splice(temp_index, 1);
				broadcastShape(JSON.stringify(shapes));
				redraw();
			}
			break;
	    }
	}
}


function updateFormulaText(latex){
  for (var shape of shapes) {
  	if (shape.textBoxId == activeFormulaId) {
  		shape.formulaLaTex = latex;
  		shape.createFormulaImage();
  		broadcastShape(JSON.stringify(shapes), "formula");
  		redraw();
  		break;
  	}
  }
}

function insertMathSymbol(){
	var math_symbol = "\\" + this.elt.value + "";

	if (activeMathField) {
		activeMathField.cmd(math_symbol);
		activeMathField.focus();
	  	updateFormulaText(activeMathField.latex());
	}
}

function updateShapes(){
	switch (mode){
	    case "select":
	      	var dx = mouseX - pmouseX;
		  	var dy = mouseY - pmouseY;

			if (selected_shape && selected_shape.dragging){
		    	selected_shape.move(dx,dy);
		    	//Send Drawing Data
		    	broadcastShape(JSON.stringify(shapes));
	      	}
	      break;

	    case "draw":
	    	switch (shape_option){
	    		case "line":
	    			var new_index = !shapes[new_shape_index] ? true : false;
	    			new_shape = shapes[new_shape_index] = new Line(startX, startY, mouseX - transX, mouseY - transY, shapeStrokeColor, lineWeightSlider.value(), false, width);
	    			if (new_index) {
	    				broadcastShape(JSON.stringify(shapes), "update");	
	    			}
	    			else{
	    				broadcastShape(JSON.stringify(shapes));
	    			}
	    			break;
	    			
	    		case "arrow":
	    			var new_index = !shapes[new_shape_index] ? true : false;
	    			new_shape = new Line(startX, startY, mouseX - transX, mouseY - transY, shapeStrokeColor, lineWeightSlider.value(), true, width);
	    			new_shape.type = "arrow";
	    			shapes[new_shape_index] = new_shape;
	    			if (new_index) {
	    				broadcastShape(JSON.stringify(shapes), "update");	
	    			}
	    			else{
	    				broadcastShape(JSON.stringify(shapes));
	    			}
	    			//broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "rect":
	    			var new_index = !shapes[new_shape_index] ? true : false;
	    			new_shape = shapes[new_shape_index] = new Rectangle(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, lineWeightSlider.value(), width);
	    			if (new_index) {
	    				broadcastShape(JSON.stringify(shapes), "update");	
	    			}
	    			else{
	    				broadcastShape(JSON.stringify(shapes));
	    			}
	    			// new_shape = shapes[new_shape_index] = new Rectangle(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, lineWeightSlider.value(), width);
	    			// broadcastShape(JSON.stringify(shapes));
	    			break;
	    		
	    		case "ellipse":
	      			var new_index = !shapes[new_shape_index] ? true : false;
	      			new_shape = shapes[new_shape_index] = new Ellipse(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, "black", lineWeightSlider.value(), width);
	      			if (new_index) {
	    				broadcastShape(JSON.stringify(shapes), "update");	
	    			}
	    			else{
	    				broadcastShape(JSON.stringify(shapes));
	    			}

	      			/*if (shapes[new_shape_index]) {
	      				new_shape = shapes[new_shape_index] = new Ellipse(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, "black", lineWeightSlider.value(), width);
	      				broadcastShape(JSON.stringify(shapes));
	      			}
	      			else{
	      				new_shape = shapes[new_shape_index] = new Ellipse(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, "black", lineWeightSlider.value(), width);
	      				broadcastShape(JSON.stringify(shapes), "update");	
	      			}*/
	      			// new_shape = shapes[new_shape_index] = new Ellipse(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, "black", lineWeightSlider.value(), width);
	      			// broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "pencil":
	    		case "highlighter":
	    			var new_index = !shapes[new_shape_index] ? true : false;

	    			new_shape.storeCoordinates(mouseX - transX, mouseY - transY, width);
	    			shapes[new_shape_index] = new_shape;
	    			if (new_index) {
	    				broadcastShape(JSON.stringify(shapes), "update");	
	    			}
	    			else{
	    				broadcastShape(JSON.stringify(shapes));
	    			}
	    			//broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "text":
			       
			        break;
			    case "formula":
			        
			        break;
	    	}
	      break;
  	}
}

function deleteShape(){
	for(var shape of shapes){
	    if (shape.selected == true) {
	      	var shape_index = shapes.indexOf(shape);
	      	shapes.splice(shape_index, 1);
	      
	      	broadcastShape(JSON.stringify(shapes), 'update');
	      	mode = "select";
	      	cursor(ARROW);
	      	redraw();
	      break;
	    }
  	}
}

function duplicateShape(){
	for(var shape of shapes){
	    if (shape.selected == true) {
	    	var duplicate_shape = new shape.constructor();
	    	for (var attr in shape) {
		        if (shape.hasOwnProperty(attr)) duplicate_shape[attr] = shape[attr];
		    }
	    	
	    	duplicate_shape.move(50,50);
	    	shapes.push(duplicate_shape);
	    	shape.selected = false;
	    	broadcastShape(JSON.stringify(shapes), "update");
	    	mode = "select";
	    	cursor(ARROW);
	    	redraw();
	      break;
	    }
  	}
}

function deselectAll(){
	for(var shape of shapes){
		shape.selected = false;
	}
	redraw();
}