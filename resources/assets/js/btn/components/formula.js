function Formula(formulaLaTex, leftX, leftY, font, fontSize, fillColor, winWidth){
	this.type = "formula";
	this.dragging = false;
  this.selected = false;
	this.formulaLaTex = formulaLaTex;
	this.leftX = leftX;
	this.leftY = leftY;
  this.rightX = null;
  this.rightY = null;
	this.font = font;
	this.fontSize = fontSize;
	this.fillColor = fillColor;
  this.winWidth = winWidth;
	this.active = true;

	this.textBoxId = Math.random().toString(36).substring(3).toUpperCase();

  this.left = null;
  this.top = null;
  this.width = null;
  this.height = null;

  this.image = null;
  this.url = null;
  this.src_base64 = null;
  this.fontLaTex = null;
  this.fontSizeLaTex = null;
}

Formula.prototype.draw = function() {
  var scale_ratio = width / this.winWidth;

  this.left = this.leftX;
  this.top = this.leftY;

  if (this.image) {
    this.width = this.image.width;
    this.height = this.image.height;  
    push();
    scale(scale_ratio);
    image(this.image, this.left, this.top);

    if (this.selected) {
      push();
      var padding = 5;
      stroke(selectShapeColor);
      strokeWeight(1);
      noFill();
      
      //Outline
      rectMode(CORNER);
      rect(this.left, this.top, this.width, this.height);
      
      //corners
      fill(selectShapeColor);
      rectMode(CORNER);
      rect(this.left, this.top, padding * 4, padding * 4);
      rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);
    }
    pop();
  }  	
};


Formula.prototype.createFormulaBox = function(/*winPosX, winPosY*/) {
   /*this.leftX = winPosX;
	 this.leftY = winPosY;
   this.left = winPosX;
   this.top = winPosY;*/
   
   activeFormulaId = this.textBoxId;
   formula_font_size = this.fontSize;

  	formulaSpan = createElement('span');
  	formulaSpan.id(this.textBoxId);
    formulaSpan.parent("canvas-region"); //may need to change to active_board_id
  	//formulaSpan.position(this.leftX, this.leftY);
    
  	formulaSpan.html(this.formulaLaTex);
  	formulaSpan.style('font-size', this.fontSize + "px");
  	formulaSpan.style('color', this.fillColor);
  	//formulaSpan.style('border', "none");
    formulaSpan.style('background-color', "#FCFBE3");
    formulaSpan.style('position', "absolute");
    formulaSpan.position(80, 200);    
  
    var formula = document.getElementById(this.textBoxId);
  	
  	var answerMathField = MQ.MathField(formula, {
	    handlers: {
	      edit: function() {
	        var enteredMath = answerMathField.latex(); // Get entered math in LaTeX format
	      }
	    }
  	});

  	formulaSpan.elt.addEventListener("focusin", function(){
    				//show formula menu
            var formula_menu = document.getElementById('formula-options');
            formula_menu.style.left = this.style.left;
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
            formula_menu.style.display = "block";
            setActiveFormulaId(this.id);            
    			});

  	formulaSpan.elt.addEventListener("keyup", function(){
            updateFormulaText(answerMathField.latex());
            
            //update location of formula menu
            var formula_menu = document.getElementById('formula-options');
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
            if (event.keyCode === 13 || event.keyCode === 27) {
                //enter or esc
                resetActiveFormulaId(this.id);
                formula_menu.style.display = "none";
                this.style.display = "none";
            }
    			});

    formulaSpan.elt.addEventListener("focusout", function(){
    				updateFormulaText(answerMathField.latex());
            //resetActiveFormulaId(this.id);
    			});
   	

    answerMathField.focus();  
   	
   	activeMathField = answerMathField;
};

Formula.prototype.locate = function(clickX, clickY) {

  var padding = 5;
  //Find Formula based on click location
  var scale_ratio = width / this.winWidth;

  scaled_left = this.left * scale_ratio;
  scaled_top = this.top * scale_ratio;
  scaled_width = this.width * scale_ratio;
  scaled_height = this.height * scale_ratio;

  if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
    //this.selected = true;
      return true;
  }
  else{
      //this.selected = false;
      return false;
  }
};

Formula.prototype.move = function(dx, dy) {
  this.leftX += dx;
  this.leftY += dy;
  this.left += dx;
  this.top += dy;

  //this.rightX += dx;
  //this.rightY += dy;

  /*var thisFormulaSpan = document.getElementById(this.textBoxId);
  thisFormulaSpan.style.left = this.leftX + "px";
  thisFormulaSpan.style.top = this.leftY + "px";*/
};

Formula.prototype.edit = function() {
  this.createFormulaBox(/*this.left, this.top*/);
  for (var shape of shapes){
    if (shape == this) {
      var shape_index = shapes.indexOf(shape);
      shapes.splice(shape_index, 1);
      shapes.push(this);
      broadcastShape(JSON.stringify(shapes), 'update');
      break;
    }
    
  }
}

Formula.prototype.createFormulaImage = function() {
  //var cog_url = "https://latex.codecogs.com/svg.latex?";
  var cog_url = "https://latex.codecogs.com/png.latex?\\dpi{300}&space;";
  
  formula_color = "\\color{" + this.fillColor + "}";

  var formula = this.fontLaTex + this.fontSizeLaTex + "{" + formula_color + this.formulaLaTex + "}";
  var cog = cog_url + encodeURIComponent(formula);
  var img = createImg(cog);
  img.hide();
  this.image = img;
  this.url = cog;
}

