function Line(startX, startY, endX, endY, strokeColor, lineWeight, arrow, winWidth){
	this.type = "line"; //changes to arrow if arrow == true
	this.dragging = false;
	this.selected = false;
	this.startX = startX;
	this.startY = startY;
	this.endX = endX;
	this.endY = endY;
	this.strokeColor = strokeColor;
	this.lineWeight = lineWeight;
	this.arrow = arrow;
	this.winWidth = winWidth;

	this.leftX = startX;
	this.leftY = startY;
	this.rightX = endX;
	this.rightY = endY;
}

Line.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	push();
	stroke(this.strokeColor);
	strokeWeight(this.lineWeight);
	
	scale(scale_ratio);

	line(this.startX, this.startY, this.endX, this.endY);
	
	//Arrow
	if (this.arrow == true) {
		var dx = this.endX - this.startX;
		var dy = this.endY - this.startY;

		var lineVector = createVector(-dx, -dy);
		lineVector.normalize();
		var arrowMag = 10;
		
		lineVector.rotate(PI / 6);
		line(this.endX, this.endY, this.endX + lineVector.x * arrowMag, this.endY + lineVector.y * arrowMag);
		lineVector.rotate(-PI / 3);
		line(this.endX, this.endY, this.endX + lineVector.x * arrowMag, this.endY + lineVector.y * arrowMag);	
	}

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.startX, this.startY, this.endX, this.endY);
		//corners
		rectMode(CENTER);
		fill(selectShapeColor);
		rect(this.startX, this.startY, padding, padding);
		rect(this.startX, this.endY, padding, padding);
		rect(this.endX, this.startY, padding, padding);
		rect(this.endX, this.endY, padding, padding);	
		pop();
	}
	pop();
};

Line.prototype.locate = function(clickX,clickY){
	var scale_ratio = width / this.winWidth;

	//create equation of line y=mx+b => y = y1 + m(x-x1)
	var slope = (this.endY - this.startY) / (this.endX - this.startX);

	var minX = min(this.startX, this.endX) * scale_ratio;
	var maxX = max(this.startX, this.endX) * scale_ratio;

	var lineY = this.startY* scale_ratio + slope*(clickX - this.startX * scale_ratio);
	var padding = 10; //check if the Y is within this many pixels

	if (clickX > minX - padding && clickX < maxX + padding && clickY > lineY - padding && clickY < lineY + padding) {
		//this.selected = true;
		return true;
	}
	else{
		//this.selected = false;
		return false;	
	}
};

Line.prototype.move = function(dx, dy) {
	this.startX += dx;
  	this.startY += dy;
  	this.endX += dx;
  	this.endY += dy;
}