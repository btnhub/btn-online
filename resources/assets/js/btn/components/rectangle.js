function Rectangle(leftX, leftY, rightX, rightY, fillColor, lineWeight, winWidth){
//function Rectangle(corner1_x, corner1_y, corner2_x, corner2_y, fillColor, lineWeight, winWidth){	
	this.type = "rect";
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = rightX;
	this.rightY = rightY;
	this.fillColor = fillColor;
	//this.strokeColor = strokeColor;
	this.lineWeight = lineWeight;
	this.winWidth = winWidth;
	this.dragging = false;
	this.selected = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Rectangle.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);

	push();
	rectMode(CORNERS);
	stroke(0);
	strokeWeight(this.lineWeight);
	
	if (this.fillColor == 'no-fill') {
		noFill();
	}
	else{
		fill(this.fillColor);	
	}

	scale(scale_ratio);
	rect(this.leftX, this.leftY, this.rightX, this.rightY);

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.leftX, this.leftY, this.rightX, this.rightY);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);

		//rectMode(CENTER);
		//rect(this.leftX, this.leftY, padding * 4, padding * 4);
		//rect(this.leftX, this.rightY, padding, padding);
		//rect(this.rightX, this.leftY, padding, padding);
		//rect(this.rightX, this.rightY, padding*4, padding*4);	
		pop();
	}
	pop();
};

Rectangle.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	//Find Rectangle based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	console.log("NW");
	    	//originalCornerX = this.leftX;
	    	//originalCornerY = this.leftY;
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	console.log("SE");
	    	//originalCornerX = this.rightX;
	    	//originalCornerY = this.rightY;
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
}

Rectangle.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;

  	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);
}

Rectangle.prototype.resize = function(finalX, finalY) {	
	var scale_ratio = width / this.winWidth;

	switch (cornerUpdated){
		case "NW":
			if (this.leftX < this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			
			break;

		case "SE":
			if (this.leftX > this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			break;
	}
}

