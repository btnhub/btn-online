function Ellipse(leftX, leftY, rightX, rightY, fillColor, strokeColor, lineWeight, winWidth){
	this.type = "ellipse";
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = rightX;
	this.rightY = rightY;
	this.fillColor = fillColor;
	this.strokeColor = strokeColor
	this.lineWeight = lineWeight;
	this.winWidth = winWidth;
	this.dragging = false;
	this.selected = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Ellipse.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);

	push();
	ellipseMode(CORNERS);
	stroke(0);
	strokeWeight(this.lineWeight);
	
	if (this.fillColor == 'no-fill') {
		noFill();
	}
	else{
		fill(this.fillColor);	
	}

	
	scale(scale_ratio);
	ellipse(this.leftX, this.leftY, this.rightX, this.rightY);

	if (this.selected) {
		push();
		//Create rectangle around ellipse
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.leftX, this.leftY, this.rightX, this.rightY);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);

		//rectMode(CENTER);
		//rect(this.leftX, this.leftY, padding * 4, padding * 4);
		//rect(this.leftX, this.rightY, padding, padding);
		//rect(this.rightX, this.leftY, padding, padding);
		//rect(this.rightX, this.rightY, padding*4, padding*4);	
		pop();
	}

	pop();
};

/*Ellipse.prototype.locate = function(clickX,clickY){
  var padding = 5;
  //Find Ellipse based on click location
  if(clickX >= this.leftX && clickX <= this.rightX && clickY >= this.leftY && clickY <= this.rightY){
    this.selected = true;
    
    //Find NW corner
    if (clickX >= (this.leftX - padding*4) && clickX <= (this.leftX + padding*4) && clickY >= (this.leftY - padding*4) && clickY <= (this.leftY + padding*4)) {
    	mode = "resize";
    	originalCornerX = this.leftX;
    	originalCornerY = this.leftY;
    	cornerUpdated = "NW";
    }
    //Find SE corner
    if (clickX >= (this.rightX - padding*4) && clickX <= (this.rightX + padding*4) && clickY >= (this.rightY - padding*4) && clickY <= (this.rightY + padding*4)) {
    	mode = "resize";
    	originalCornerX = this.rightX;
    	originalCornerY = this.rightY;
    	cornerUpdated = "SE";
    }

    return true;
  }
  else{
  	this.selected = false;
    return false;
  }
};*/

Ellipse.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find Ellipse based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
}

Ellipse.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;

  	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);
}

Ellipse.prototype.resize = function(finalX, finalY) {	
	var scale_ratio = width / this.winWidth;

	switch (cornerUpdated){
		case "NW":
			if (this.leftX < this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			
			break;

		case "SE":
			if (this.leftX > this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			break;
	}
}
