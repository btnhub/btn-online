function Image(image, leftX, leftY, width, height, winWidth){
	this.type = "image";
	this.image = image;
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = leftX + width;
	this.rightY = leftY + height;
	this.width = width;
	this.height = height;
	this.winWidth = winWidth;
	this.dragging = false;

	this.src_base64 = null;
	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Image.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = this.leftX;
	this.top = this.leftY;

	if (this.width == 0 || this.width == null) {
		this.width = this.image.width;
		this.height = this.image.height;
	}

	push();
	scale(scale_ratio);
	image(this.image, this.leftX, this.leftY, this.width, this.height);

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNER);
		rect(this.left, this.top, this.width, this.height);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);
		pop();
	}
	pop();
}

Image.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find  based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;
		console.log("found");
	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	    return false;
	}
}

Image.prototype.move = function(dx, dy) {
	this.leftX += dx;
	this.leftY += dy;
	this.rightX = this.leftX + this.width;
	this.rightY = this.leftY + this.height;

	this.left = this.leftX;
	this.top = this.leftY;
}

Image.prototype.resize = function(finalX, finalY) {
	var scale_ratio = width / this.winWidth;
	var min_size = 100;

	switch (cornerUpdated){
		case "NW":
			new_left = finalX / scale_ratio;
			new_top = finalY / scale_ratio;	
			new_width = this.width - (new_left - this.leftX);
			new_height = this.height - (new_top - this.leftY);
			
			if (new_width >= min_size && new_height >= min_size) {
				this.left = this.leftX = new_left;
				this.top = this.leftY = new_top;

				this.width = new_width;
				this.height = new_height;
			}
		break;

		case "SE":
			new_right = finalX / scale_ratio;
			new_bottom = finalY / scale_ratio;	
			new_width = this.width + (new_right - this.rightX);
			new_height = this.height + (new_bottom - this.rightY);
			
			if (new_width >= min_size && new_height >= min_size) {
				this.rightX = new_right;
				this.rightY = new_bottom;

				this.width = new_width;
				this.height = new_height;
			}
			break;
	}
}

