function Pencil(radius, fillColor, winWidth){
	this.type = "pencil";
	this.dragging = false;
	this.selected = false;
	this.drawing = false;
	this.radius = radius;
	this.fillColor = fillColor;
	this.winWidth = winWidth;
	this.coordinates = '';
	this.minX = null;
	this.minY = null;
	this.maxX = null;
	this.maxY = null;
	this.leftX = null;
	this.leftY = null;
	this.rightX = null;
	this.rightY = null;
	this.highlighter = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Pencil.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	var coordX = [];
	var coordY = [];
	
	push();
	scale(scale_ratio);
	
	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = coordinate_pairs[i].split(",")[0];
	    	coordY[i] = coordinate_pairs[i].split(",")[1];
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	if (i == 0) {
		    		noStroke();
			  	    fill(this.fillColor);	
			  	  	ellipse(coordX[i], coordY[i], this.radius, this.radius);
		    	}
		    	else {
	    	  		noStroke();
		        	fill(this.fillColor);	
			      	ellipse(coordX[i], coordY[i], this.radius, this.radius);	
			        stroke(this.fillColor);
			        strokeWeight(this.radius * 2); 
			        line(coordX[i], coordY[i], coordX[i-1], coordY[i-1]);
		      	}	
	    	}
	  	}

	  	//console.log(coordX);
	  	coordX.pop();
	  	coordY.pop();
  		this.minX = this.leftX = min(coordX);
	  	this.maxX = this.rightX = max(coordX);
	  	this.minY = this.leftY = min(coordY);
	  	this.maxY = this.rightY = max(coordY);

	  	this.left = this.minX;
		this.top = this.minY;
		this.width = Math.abs(this.maxX - this.minX);
		this.height = Math.abs(this.maxY - this.minY);

	  	if (this.selected) {
	  		push();
			var padding = 5;
			stroke(selectShapeColor);
			strokeWeight(2);
			noFill();
			//Outline
			rectMode(CORNERS);
			rect(this.minX, this.minY, this.maxX, this.maxY);
			//corners
			fill(selectShapeColor);
			rectMode(CENTER);
			rect(this.minX, this.minY, padding, padding);
			rect(this.minX, this.maxY, padding, padding);
			rect(this.maxX, this.minY, padding, padding);
			rect(this.maxX, this.maxY, padding, padding);	
			pop();
	  	}
	}
	pop();
};

Pencil.prototype.locate = function(clickX, clickY) {
  	var coordX = [];
	var coordY = [];
	var padding = 20; //if cursor is within 20px
	var scale_ratio = width / this.winWidth;

  	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	
	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = Number(coordinate_pairs[i].split(",")[0]) * scale_ratio;
	    	coordY[i] = Number(coordinate_pairs[i].split(",")[1]) * scale_ratio;
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	if (clickX > coordX[i] - padding && clickX < coordX[i] + padding
		    		&& clickY > coordY[i] - padding && clickY < coordY[i] + padding ){
		    		
    				return true;
		    	}	
	    	}
	  	}
	}

    return false;
};

Pencil.prototype.move = function(dx, dy) {
	var coordX = [];
	var coordY = [];
  	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	this.coordinates = ""; //reset coordinates

	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = coordinate_pairs[i].split(",")[0];
	    	coordY[i] = coordinate_pairs[i].split(",")[1];
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	coordX[i] = parseFloat(coordX[i]) + dx;
		    	coordY[i] = parseFloat(coordY[i]) + dy;
		    	this.storeCoordinates(coordX[i], coordY[i]);
	    	}
	  	}
	}
};

Pencil.prototype.storeCoordinates = function(coordX, coordY) {
	this.coordinates += "" + coordX + "," + coordY + ";";
};


