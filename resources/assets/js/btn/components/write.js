function Write(textString, leftX, leftY, font, fontSize, fillColor, winWidth){
	this.type = "text";
	this.dragging = false;
	this.textString = textString;
	this.leftX = leftX;
	this.leftY = leftY;
	this.font = font;
	this.fontSize = fontSize;
	this.fillColor = fillColor;
	this.winWidth = winWidth;

	this.formattedString = '';
	this.show = true;
	this.textBoxId = Math.random().toString(36).substring(3).toUpperCase();
	this.textBoxWidth = 300;
	this.textBoxHeight = 50;

	this.rightX = leftX + this.textBoxWidth;
  	this.rightY = leftY + this.textBoxHeight;

  	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Write.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = this.leftX;
	this.top = this.leftY;
	this.width = this.textBoxWidth;
	this.height = this.textBoxHeight;

	push();
	if (this.show) {
		rectMode(CORNER);
		fill(this.fillColor);
		noStroke();
		textFont(this.font);
		textSize(this.fontSize);
		
		scale(scale_ratio);
		text(this.formattedString, this.left, this.top, this.width);	
		
		if (this.selected) {
			push();
			var padding = 5;
			var cornerLeftX = this.left - padding;
			var cornerLeftY = this.top - padding;
			var cornerRightX = this.left + this.width + padding;
			var cornerRightY = this.top + this.height + padding;

			stroke(selectShapeColor);
			strokeWeight(1);
			noFill();
			//Outline
			rectMode(CORNER);
			//rect(cornerLeftX, cornerLeftY, cornerRightX , cornerRightY);
			rect(this.left - padding, this.top - this.fontSize - padding, this.width + padding , this.height + padding);

			//corners
			fill(selectShapeColor);
			rectMode(CORNER);
			//rect(this.left, this.top, padding * 4, padding * 4);
			rect(this.left + this.width - (padding * 4), this.top + this.height - this.fontSize - (padding*4), padding * 4, padding * 4);

			pop();
		}
	}
	pop();
};


Write.prototype.createTextBox = function(winPosX, winPosY) {
  	this.show = false;
  	
  	var tempTextArea = createElement('textArea');
  	tempTextArea.id(this.textBoxId);
  	tempTextArea.input(updateText);
  	tempTextArea.position(winPosX, winPosY);
    tempTextArea.size(this.textBoxWidth, this.textBoxHeight);
    tempTextArea.value(this.textString);
    tempTextArea.elt.blur();
    tempTextArea.elt.focus();
    tempTextArea.elt.addEventListener("focusout", function(){removeTextBox(tempTextArea.id())});

    tempTextArea.style('background-color', "rgba(255,255,255,0)"); //transparent background
    tempTextArea.style('color', this.fillColor);
    tempTextArea.style('font-size', this.fontSize + "px");
    tempTextArea.style('font-family', this.font);
    tempTextArea.style('border', "1px dashed gray");
};

Write.prototype.resize = function(finalX, finalY) {
	var scale_ratio = width / this.winWidth;

	var new_width = Math.abs(this.leftX - finalX / scale_ratio);

	if (new_width > 200) {
		switch (cornerUpdated){
			case "NW":
				break;

			case "SE":
				break;
		}

		this.textBoxWidth = new_width;
		this.fitToBox(this.textBoxWidth);	
	}	
};

Write.prototype.removeTextBox = function() {
	this.show = true;
	document.getElementById(this.textBoxId).remove();
};

Write.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find Write based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = (this.top - this.fontSize) * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
};

Write.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;
};

Write.prototype.fitToBox = function(textBoxWidth) {
	var leftOverText = this.textString;
	var formattedString = '';
  
  	var count = 0;
	var lines = this.textString.split('\n');
	var textBoxHeight = 0;

	for (var j = 0; j < lines.length; j++) {
		if (textWidth(lines[j]) > textBoxWidth) {
			var words = lines[j].split(" ");
			var new_line = "";

			textBoxHeight += (this.fontSize + textLeading());

			for (var i = 0; i < words.length; i++) {
				if (textWidth(new_line + words[i] + " ") < textBoxWidth) {
					new_line += words[i] + " ";
					formattedString += words[i] + " ";
				}
				else {
					formattedString += words[i] + "\n";
					/*var*/ new_line = "";
					textBoxHeight += (this.fontSize + textLeading());
				}
			}
		}
		else {
			formattedString += lines[j] + '\n';
			textBoxHeight += (this.fontSize + textLeading());
		}
	}
	
	textBoxHeight += (this.fontSize + textLeading());

	this.formattedString = formattedString;
	this.textBoxWidth = textBoxWidth;
	this.textBoxHeight = textBoxHeight > 50 ? textBoxHeight : 50;

	this.rightX = this.leftX + this.textBoxWidth;
  	this.rightY = this.leftY + this.textBoxHeight;

  	this.width = this.textBoxWidth;
	this.height = this.textBoxHeight;
};

Write.prototype.edit = function() {
	this.show = false;
	this.createTextBox(this.left, this.top);
}