function createAddTab(parent_element){
	var add_tab_item = createElement("li")
	add_tab_item.id("add_tab")
	add_tab_item.parent(parent_element);
		
	var add_tab_link = createA("#", "+ Add Tab");
	add_tab_link.parent(add_tab_item);
	//board_count = selectAll(".boards").length;

	//add_tab_link.mouseClicked(sayHi(board_count++));
	//add_tab_link.mouseClicked(createTab());
}

function createTab(){
	//Find and remove Add Tab 
	var add_tab = select("#add_tab");
	if (add_tab) {
		add_tab.remove();	
	}
	
	var board_number = selectAll(".boards").length + 1;
	var tab = createElement("ul");
	tab.class("nav nav-tabs");
	tab.parent('canvas-region');

	var tab_item = createElement("li")
	tab_item.parent(tab);
	tab_item.id("li"+board_number);
	tab_item.class("active");

	var tab_id = "tab" + board_number;
	
	var tab_link = createA("#" + tab_id, "Board " + board_number);
	tab_link.attribute('data-toggle', "tab");
	tab_link.parent(tab_item);

	var content = createDiv();
	content.class("tab-content");
	content.parent('canvas-region');

	var tab_content_div = createDiv();
	tab_content_div.id(tab_id);
	tab_content_div.class("tab-pane fade in active show");	
	tab_content_div.parent(content);

	var tab_content = createDiv();
	tab_content.id("board" + board_number);
	tab_content.parent(tab_content_div);
	tab_content.class("boards");
	canvas = createCanvas(windowWidth, 800);
	canvas.parent(tab_content);

	//Add Tab
	createAddTab(tab);

	//TEMP: To Test Empty Tabs
	for (var i = 2; i < 5; i++) {
		//Find and remove Add Tab 
		var add_tab = select("#add_tab");
		if (add_tab) {
			add_tab.remove();	
		}
		tab_id = "tab" + i;

		tab_item = createElement("li")
		tab_item.parent(tab);
		
		tab_link = createA("#" + tab_id, "Board " + i);
		tab_link.attribute('data-toggle', "tab");
		tab_link.parent(tab_item);

		tab_content_div = createDiv();
		tab_content_div.id(tab_id);
		tab_content_div.class("tab-pane fade");
		tab_content_div.parent(content);

		tab_content = createDiv();
		tab_content.id("board" + i);
		tab_content.parent(tab_content_div);
		tab_content.class("boards");
		
		//Add Tab
		createAddTab(tab);
	}
}

function deleteTab(board_number){
	//remove element with id "li"+board_number & "tab"+board_number
}

function sayHi(board_number){
	console.log(board_number);
}