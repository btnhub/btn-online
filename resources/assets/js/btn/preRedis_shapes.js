function selectShape(chosen_shape){
	mode = "draw";
	cursor(CROSS);

	shape_option = chosen_shape;
	
	if (shape_option == 'text') {
		cursor(TEXT);
	}
}

function receiveShapes(data){
	//turn data into objects
	var received_shapes = [];
	for (var i = 0; i < data.length; i++) {
		switch (data[i].type){
			case "line":
				received_shapes[i] = new Line(data[i].startX, data[i].startY, data[i].endX, data[i].endY, data[i].strokeColor, data[i].lineWeight, false, data[i].winWidth);
				break;

			case "arrow":
				received_shapes[i] = new Line(data[i].startX, data[i].startY, data[i].endX, data[i].endY, data[i].strokeColor, data[i].lineWeight, true, data[i].winWidth);
				received_shapes[i].type = "arrow";
				break;

			case "rect":
				received_shapes[i] = new Rectangle(data[i].leftX, data[i].leftY, data[i].rightX, data[i].rightY, data[i].fillColor, data[i].lineWeight, data[i].winWidth);
				break;

			case "ellipse":
				received_shapes[i] = new Ellipse(data[i].leftX, data[i].leftY, data[i].rightX, data[i].rightY, data[i].fillColor, data[i].strokeColor, data[i].lineWeight, data[i].winWidth);
				break;

			case "pencil":
				received_shapes[i] = new Pencil(data[i].radius, data[i].fillColor, data[i].winWidth);
				received_shapes[i].coordinates = data[i].coordinates;
				break;

			case "text":
				received_shapes[i] = new Write(data[i].textString, data[i].leftX, data[i].leftY, data[i].font, data[i].fontSize, data[i].fillColor, data[i].winWidth);
				received_shapes[i].formattedString = data[i].formattedString;
				break;

			case "formula":
				received_shapes[i] = new Formula(data[i].formulaLaTex, data[i].leftX, data[i].leftY, data[i].font, data[i].fontSize, data[i].fillColor, data[i].winWidth);
				received_shapes[i].textBoxId = data[i].textBoxId;
				break;

			case "image":
				var img = createImg(data[i].src_base64);
				img.hide();
				received_shapes[i] = new Image(img, data[i].leftX, data[i].leftY, data[i].width, data[i].height, data[i].winWidth);
    			received_shapes[i].src_base64 = img.elt.src;
				break;
		}
	}
	shapes = received_shapes;
}

function findShape(){	  
  drag_canvas = true;
  for (var shape of shapes){
  	if (typeof shape.locate !== "undefined") {
  		if(shape.locate(mouseX, mouseY)){
	      selected_shape = shape;
	      originalLeftX = selected_shape.leftX;
	      originalLeftY = selected_shape.leftY;
	      originalRightX = selected_shape.rightX;
	      originalRightY = selected_shape.rightY;
	      selected_shape.drag = true;
	      drag_canvas = false;
	      break;
	    }	
  	}
    else
    {
    	//Find box containing shape (rect, ellipse, write, formula)
    	if(mouseX - transX >= shape.leftX && mouseX - transX <= shape.rightX && mouseY - transY >= shape.leftY && mouseY - transY <= shape.rightY){
			var padding = 5;
		    shape.selected = true;
		    selected_shape = shape;
		    
		    //Find NW Corner
		    if (mouseX - transX >= (shape.leftX - padding*4) && mouseX - transX <= (shape.leftX + padding*4) && mouseY - transY >= (shape.leftY - padding*4) && mouseY - transY <= (shape.leftY + padding*4)) {
		    	mode = "resize";
		    	originalCornerX = shape.leftX;
		    	originalCornerY = shape.leftY;
		    	cornerUpdated = "NW";
		    }
		    
		    //Find SE Corner
		    if (mouseX - transX >= (shape.rightX - padding*4) && mouseX - transX <= (shape.rightX + padding*4) && mouseY - transY >= (shape.rightY - padding*4) && mouseY - transY <= (shape.rightY + padding*4)) {
		    	mode = "resize";
		    	originalCornerX = shape.rightX;
		    	originalCornerY = shape.rightY;
		    	cornerUpdated = "SE";
		    }

		    originalLeftX = selected_shape.leftX;
		    originalLeftY = selected_shape.leftY;
		    originalRightX = selected_shape.rightX;
		    originalRightY = selected_shape.rightY;
		    selected_shape.drag = true;
		    drag_canvas = false;
		    break;
		    //return true;
		}
		else{
		  	shape.selected = false;
		    //return false;
		}
    }
  }
}

function updateText(){
  new_text.textString = this.value();
  new_text.fitToBox(this.width);

  shapes[new_shape_index] = new_text;
  broadcastShape(JSON.stringify(shapes));
  //socket.emit('shapes', shapes);
}

function removeTextBox(textBoxId){
	var textBox = document.getElementById(textBoxId);
	
	for (var shape of shapes){
		if (shape.type == "text" && shape.textBoxId == textBoxId) {
			if (textBox.value && textBox.value.trim() != '') {
				shape.show = true;
				shape.draw();
			}
			else{
				//delete empty Text from shapes
				var temp_index = shapes.indexOf(shape);
				if (temp_index != -1) {
					shapes.splice(temp_index, 1);
				}
			}
	    }
	}
	textBox.remove();
}

function setActiveFormulaId(formulaSpanId){
	activeFormulaId = formulaSpanId;
}

function resetActiveFormulaId(){
	activeFormulaId = null;
}


function updateFormulaText(latex){
  for (var shape of shapes) {
  	if (shape.textBoxId == activeFormulaId) {
  		shape.formulaLaTex = latex;
  		break;
  	}
  }
  broadcastShape(JSON.stringify(shapes));
}

function insertMathSymbol(){
	var math_symbol = "\\" + this.elt.value + "";

	if (activeMathField) {
		activeMathField.cmd(math_symbol);
		activeMathField.focus();
	  	updateFormulaText(activeMathField.latex());
	}
}

function updateShapes(){
	switch (mode){
	    case "drag":
	      var dx = mouseX - pmouseX;
		  var dy = mouseY - pmouseY;

	      if (drag_canvas) {
	      	if (keyIsDown) {
	      		cursor(MOVE);	
	      	}

	      	transX += dx;
	      	transY += dy;
	      }	

	      else if (selected_shape && selected_shape.drag){
		      selected_shape.move(dx,dy);
		      //Send Drawing Data
		      broadcastShape(JSON.stringify(shapes));
	      }
	      break;

	    case "draw":
	    	switch (shape_option){
	    		case "line":
	      			new_line = shapes[new_shape_index] = new Line(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, lineWeightSlider.value(), false, width);
	    			new_line.selected = true;
	    			broadcastShape(JSON.stringify(shapes));
	    			break;
	    			
	    		case "arrow":
	      			new_arrow = new Line(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, lineWeightSlider.value(), true, width);
	    			new_arrow.type = "arrow";
	    			new_arrow.selected = true;
	    			shapes[new_shape_index] = new_arrow;
	    			broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "rect":
	    			new_rect = new Rectangle(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, lineWeightSlider.value(), width);
	      			new_rect.selected = true;
	      			shapes[new_shape_index] = new_rect;
	    			
	    			broadcastShape(JSON.stringify(shapes));
	    			break;
	    		
	    		case "ellipse":
	    			new_ellipse = new Ellipse(startX, startY, mouseX - transX, mouseY - transY, shapeFillColor, "black", lineWeightSlider.value(), width);
	      			new_ellipse.selected = true;
	      			shapes[new_shape_index] = new_ellipse;

	      			broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "pencil":
	    		case "highlighter":
	    			//translate(-transX, -transY);
	    			new_pencil.storeCoordinates(mouseX - transX, mouseY - transY, width);
	    			shapes[new_shape_index] = new_pencil;
	    			broadcastShape(JSON.stringify(shapes));
	    			break;

	    		case "text":
			        //new_text.createTextBox(winMouseX - transX, winmouseY - transY);
			        break;
			    case "formula":
			        //new_formula.createFormulaBox(winMouseX - transX, winmouseY - transY);
			        break;
	    	}
	      break;
  	}
}

function deleteShape(){
	for(var shape of shapes){
	    if (shape.selected == true) {
	      var shape_index = shapes.indexOf(shape);
	      undone_shapes.push(shapes.splice(shape_index));
	      broadcastShape(JSON.stringify(shapes));
	      break;
	    }
  	}
}

function undeleteShape(){
	/*if (undone_shapes.length) {
		shapes.push(undone_shapes.pop());  
		broadcastShape(JSON.stringify(shapes));
	}*/
}