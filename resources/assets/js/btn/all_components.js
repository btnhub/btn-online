function Ellipse(leftX, leftY, rightX, rightY, fillColor, strokeColor, lineWeight, winWidth){
	this.type = "ellipse";
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = rightX;
	this.rightY = rightY;
	this.fillColor = fillColor;
	this.strokeColor = strokeColor
	this.lineWeight = lineWeight;
	this.winWidth = winWidth;
	this.dragging = false;
	this.selected = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Ellipse.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);

	push();
	ellipseMode(CORNERS);
	stroke(0);
	strokeWeight(this.lineWeight);
	
	if (this.fillColor == 'no-fill') {
		noFill();
	}
	else{
		fill(this.fillColor);	
	}

	
	scale(scale_ratio);
	ellipse(this.leftX, this.leftY, this.rightX, this.rightY);

	if (this.selected) {
		push();
		//Create rectangle around ellipse
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.leftX, this.leftY, this.rightX, this.rightY);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);

		//rectMode(CENTER);
		//rect(this.leftX, this.leftY, padding * 4, padding * 4);
		//rect(this.leftX, this.rightY, padding, padding);
		//rect(this.rightX, this.leftY, padding, padding);
		//rect(this.rightX, this.rightY, padding*4, padding*4);	
		pop();
	}

	pop();
};

/*Ellipse.prototype.locate = function(clickX,clickY){
  var padding = 5;
  //Find Ellipse based on click location
  if(clickX >= this.leftX && clickX <= this.rightX && clickY >= this.leftY && clickY <= this.rightY){
    this.selected = true;
    
    //Find NW corner
    if (clickX >= (this.leftX - padding*4) && clickX <= (this.leftX + padding*4) && clickY >= (this.leftY - padding*4) && clickY <= (this.leftY + padding*4)) {
    	mode = "resize";
    	originalCornerX = this.leftX;
    	originalCornerY = this.leftY;
    	cornerUpdated = "NW";
    }
    //Find SE corner
    if (clickX >= (this.rightX - padding*4) && clickX <= (this.rightX + padding*4) && clickY >= (this.rightY - padding*4) && clickY <= (this.rightY + padding*4)) {
    	mode = "resize";
    	originalCornerX = this.rightX;
    	originalCornerY = this.rightY;
    	cornerUpdated = "SE";
    }

    return true;
  }
  else{
  	this.selected = false;
    return false;
  }
};*/

Ellipse.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find Ellipse based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
}

Ellipse.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;

  	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);
}

Ellipse.prototype.resize = function(finalX, finalY) {	
	var scale_ratio = width / this.winWidth;

	switch (cornerUpdated){
		case "NW":
			if (this.leftX < this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			
			break;

		case "SE":
			if (this.leftX > this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			break;
	}
}

function Formula(formulaLaTex, leftX, leftY, font, fontSize, fillColor, winWidth){
	this.type = "formula";
	this.dragging = false;
  this.selected = false;
	this.formulaLaTex = formulaLaTex;
	this.leftX = leftX;
	this.leftY = leftY;
  this.rightX = null;
  this.rightY = null;
	this.font = font;
	this.fontSize = fontSize;
	this.fillColor = fillColor;
  this.winWidth = winWidth;
	this.active = true;

	this.textBoxId = Math.random().toString(36).substring(3).toUpperCase();

  this.left = null;
  this.top = null;
  this.width = null;
  this.height = null;

  this.image = null;
  this.url = null;
  this.src_base64 = null;
  this.fontLaTex = null;
  this.fontSizeLaTex = null;
}

Formula.prototype.draw = function() {
  var scale_ratio = width / this.winWidth;

  this.left = this.leftX;
  this.top = this.leftY;

  if (this.image) {
    this.width = this.image.width;
    this.height = this.image.height;  
    push();
    scale(scale_ratio);
    image(this.image, this.left, this.top);

    if (this.selected) {
      push();
      var padding = 5;
      stroke(selectShapeColor);
      strokeWeight(1);
      noFill();
      
      //Outline
      rectMode(CORNER);
      rect(this.left, this.top, this.width, this.height);
      
      //corners
      fill(selectShapeColor);
      rectMode(CORNER);
      rect(this.left, this.top, padding * 4, padding * 4);
      rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);
    }
    pop();
  }  	
};


Formula.prototype.createFormulaBox = function(/*winPosX, winPosY*/) {
   /*this.leftX = winPosX;
	 this.leftY = winPosY;
   this.left = winPosX;
   this.top = winPosY;*/
   
   activeFormulaId = this.textBoxId;
   formula_font_size = this.fontSize;

  	formulaSpan = createElement('span');
  	formulaSpan.id(this.textBoxId);
    formulaSpan.parent("canvas-region"); //may need to change to active_board_id
  	//formulaSpan.position(this.leftX, this.leftY);
    
  	formulaSpan.html(this.formulaLaTex);
  	formulaSpan.style('font-size', this.fontSize + "px");
  	formulaSpan.style('color', this.fillColor);
  	//formulaSpan.style('border', "none");
    formulaSpan.style('background-color', "#FCFBE3");
    formulaSpan.style('position', "absolute");
    formulaSpan.position(80, 200);    
  
    var formula = document.getElementById(this.textBoxId);
  	
  	var answerMathField = MQ.MathField(formula, {
	    handlers: {
	      edit: function() {
	        var enteredMath = answerMathField.latex(); // Get entered math in LaTeX format
	      }
	    }
  	});

  	formulaSpan.elt.addEventListener("focusin", function(){
    				//show formula menu
            var formula_menu = document.getElementById('formula-options');
            formula_menu.style.left = this.style.left;
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
            formula_menu.style.display = "block";
            setActiveFormulaId(this.id);            
    			});

  	formulaSpan.elt.addEventListener("keyup", function(){
            updateFormulaText(answerMathField.latex());
            
            //update location of formula menu
            var formula_menu = document.getElementById('formula-options');
            formula_menu.style.top = parseInt(this.style.top) + parseInt(this.offsetHeight) + 10 + "px";
            if (event.keyCode === 13 || event.keyCode === 27) {
                //enter or esc
                resetActiveFormulaId(this.id);
                formula_menu.style.display = "none";
                this.style.display = "none";
            }
    			});

    formulaSpan.elt.addEventListener("focusout", function(){
    				updateFormulaText(answerMathField.latex());
            //resetActiveFormulaId(this.id);
    			});
   	

    answerMathField.focus();  
   	
   	activeMathField = answerMathField;
};

Formula.prototype.locate = function(clickX, clickY) {

  var padding = 5;
  //Find Formula based on click location
  var scale_ratio = width / this.winWidth;

  scaled_left = this.left * scale_ratio;
  scaled_top = this.top * scale_ratio;
  scaled_width = this.width * scale_ratio;
  scaled_height = this.height * scale_ratio;

  if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
    //this.selected = true;
      return true;
  }
  else{
      //this.selected = false;
      return false;
  }
};

Formula.prototype.move = function(dx, dy) {
  this.leftX += dx;
  this.leftY += dy;
  this.left += dx;
  this.top += dy;

  //this.rightX += dx;
  //this.rightY += dy;

  /*var thisFormulaSpan = document.getElementById(this.textBoxId);
  thisFormulaSpan.style.left = this.leftX + "px";
  thisFormulaSpan.style.top = this.leftY + "px";*/
};

Formula.prototype.edit = function() {
  this.createFormulaBox(/*this.left, this.top*/);
  for (var shape of shapes){
    if (shape == this) {
      var shape_index = shapes.indexOf(shape);
      shapes.splice(shape_index, 1);
      shapes.push(this);
      broadcastShape(JSON.stringify(shapes), 'update');
      break;
    }
    
  }
}

Formula.prototype.createFormulaImage = function() {
  //var cog_url = "https://latex.codecogs.com/svg.latex?";
  var cog_url = "https://latex.codecogs.com/png.latex?\\dpi{300}&space;";
  
  formula_color = "\\color{" + this.fillColor + "}";

  var formula = this.fontLaTex + this.fontSizeLaTex + "{" + formula_color + this.formulaLaTex + "}";
  var cog = cog_url + encodeURIComponent(formula);
  var img = createImg(cog);
  img.hide();
  this.image = img;
  this.url = cog;
}


function Image(image, leftX, leftY, width, height, winWidth){
	this.type = "image";
	this.image = image;
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = leftX + width;
	this.rightY = leftY + height;
	this.width = width;
	this.height = height;
	this.winWidth = winWidth;
	this.dragging = false;

	this.src_base64 = null;
	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Image.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = this.leftX;
	this.top = this.leftY;

	if (this.width == 0 || this.width == null) {
		this.width = this.image.width;
		this.height = this.image.height;
	}

	push();
	scale(scale_ratio);
	image(this.image, this.leftX, this.leftY, this.width, this.height);

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNER);
		rect(this.left, this.top, this.width, this.height);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);
		pop();
	}
	pop();
}

Image.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find  based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;
		console.log("found");
	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	    return false;
	}
}

Image.prototype.move = function(dx, dy) {
	this.leftX += dx;
	this.leftY += dy;
	this.rightX = this.leftX + this.width;
	this.rightY = this.leftY + this.height;

	this.left = this.leftX;
	this.top = this.leftY;
}

Image.prototype.resize = function(finalX, finalY) {
	var scale_ratio = width / this.winWidth;
	var min_size = 100;

	switch (cornerUpdated){
		case "NW":
			new_left = finalX / scale_ratio;
			new_top = finalY / scale_ratio;	
			new_width = this.width - (new_left - this.leftX);
			new_height = this.height - (new_top - this.leftY);
			
			if (new_width >= min_size && new_height >= min_size) {
				this.left = this.leftX = new_left;
				this.top = this.leftY = new_top;

				this.width = new_width;
				this.height = new_height;
			}
		break;

		case "SE":
			new_right = finalX / scale_ratio;
			new_bottom = finalY / scale_ratio;	
			new_width = this.width + (new_right - this.rightX);
			new_height = this.height + (new_bottom - this.rightY);
			
			if (new_width >= min_size && new_height >= min_size) {
				this.rightX = new_right;
				this.rightY = new_bottom;

				this.width = new_width;
				this.height = new_height;
			}
			break;
	}
}


function Line(startX, startY, endX, endY, strokeColor, lineWeight, arrow, winWidth){
	this.type = "line"; //changes to arrow if arrow == true
	this.dragging = false;
	this.selected = false;
	this.startX = startX;
	this.startY = startY;
	this.endX = endX;
	this.endY = endY;
	this.strokeColor = strokeColor;
	this.lineWeight = lineWeight;
	this.arrow = arrow;
	this.winWidth = winWidth;

	this.leftX = startX;
	this.leftY = startY;
	this.rightX = endX;
	this.rightY = endY;
}

Line.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	push();
	stroke(this.strokeColor);
	strokeWeight(this.lineWeight);
	
	scale(scale_ratio);

	line(this.startX, this.startY, this.endX, this.endY);
	
	//Arrow
	if (this.arrow == true) {
		var dx = this.endX - this.startX;
		var dy = this.endY - this.startY;

		var lineVector = createVector(-dx, -dy);
		lineVector.normalize();
		var arrowMag = 10;
		
		lineVector.rotate(PI / 6);
		line(this.endX, this.endY, this.endX + lineVector.x * arrowMag, this.endY + lineVector.y * arrowMag);
		lineVector.rotate(-PI / 3);
		line(this.endX, this.endY, this.endX + lineVector.x * arrowMag, this.endY + lineVector.y * arrowMag);	
	}

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.startX, this.startY, this.endX, this.endY);
		//corners
		rectMode(CENTER);
		fill(selectShapeColor);
		rect(this.startX, this.startY, padding, padding);
		rect(this.startX, this.endY, padding, padding);
		rect(this.endX, this.startY, padding, padding);
		rect(this.endX, this.endY, padding, padding);	
		pop();
	}
	pop();
};

Line.prototype.locate = function(clickX,clickY){
	var scale_ratio = width / this.winWidth;

	//create equation of line y=mx+b => y = y1 + m(x-x1)
	var slope = (this.endY - this.startY) / (this.endX - this.startX);

	var minX = min(this.startX, this.endX) * scale_ratio;
	var maxX = max(this.startX, this.endX) * scale_ratio;

	var lineY = this.startY* scale_ratio + slope*(clickX - this.startX * scale_ratio);
	var padding = 10; //check if the Y is within this many pixels

	if (clickX > minX - padding && clickX < maxX + padding && clickY > lineY - padding && clickY < lineY + padding) {
		//this.selected = true;
		return true;
	}
	else{
		//this.selected = false;
		return false;	
	}
};

Line.prototype.move = function(dx, dy) {
	this.startX += dx;
  	this.startY += dy;
  	this.endX += dx;
  	this.endY += dy;
}
function Pencil(radius, fillColor, winWidth){
	this.type = "pencil";
	this.dragging = false;
	this.selected = false;
	this.drawing = false;
	this.radius = radius;
	this.fillColor = fillColor;
	this.winWidth = winWidth;
	this.coordinates = '';
	this.minX = null;
	this.minY = null;
	this.maxX = null;
	this.maxY = null;
	this.leftX = null;
	this.leftY = null;
	this.rightX = null;
	this.rightY = null;
	this.highlighter = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Pencil.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	var coordX = [];
	var coordY = [];
	
	push();
	scale(scale_ratio);
	
	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = coordinate_pairs[i].split(",")[0];
	    	coordY[i] = coordinate_pairs[i].split(",")[1];
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	if (i == 0) {
		    		noStroke();
			  	    fill(this.fillColor);	
			  	  	ellipse(coordX[i], coordY[i], this.radius, this.radius);
		    	}
		    	else {
	    	  		noStroke();
		        	fill(this.fillColor);	
			      	ellipse(coordX[i], coordY[i], this.radius, this.radius);	
			        stroke(this.fillColor);
			        strokeWeight(this.radius * 2); 
			        line(coordX[i], coordY[i], coordX[i-1], coordY[i-1]);
		      	}	
	    	}
	  	}

	  	//console.log(coordX);
	  	coordX.pop();
	  	coordY.pop();
  		this.minX = this.leftX = min(coordX);
	  	this.maxX = this.rightX = max(coordX);
	  	this.minY = this.leftY = min(coordY);
	  	this.maxY = this.rightY = max(coordY);

	  	this.left = this.minX;
		this.top = this.minY;
		this.width = Math.abs(this.maxX - this.minX);
		this.height = Math.abs(this.maxY - this.minY);

	  	if (this.selected) {
	  		push();
			var padding = 5;
			stroke(selectShapeColor);
			strokeWeight(2);
			noFill();
			//Outline
			rectMode(CORNERS);
			rect(this.minX, this.minY, this.maxX, this.maxY);
			//corners
			fill(selectShapeColor);
			rectMode(CENTER);
			rect(this.minX, this.minY, padding, padding);
			rect(this.minX, this.maxY, padding, padding);
			rect(this.maxX, this.minY, padding, padding);
			rect(this.maxX, this.maxY, padding, padding);	
			pop();
	  	}
	}
	pop();
};

Pencil.prototype.locate = function(clickX, clickY) {
  	var coordX = [];
	var coordY = [];
	var padding = 20; //if cursor is within 20px
	var scale_ratio = width / this.winWidth;

  	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	
	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = Number(coordinate_pairs[i].split(",")[0]) * scale_ratio;
	    	coordY[i] = Number(coordinate_pairs[i].split(",")[1]) * scale_ratio;
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	if (clickX > coordX[i] - padding && clickX < coordX[i] + padding
		    		&& clickY > coordY[i] - padding && clickY < coordY[i] + padding ){
		    		
    				return true;
		    	}	
	    	}
	  	}
	}

    return false;
};

Pencil.prototype.move = function(dx, dy) {
	var coordX = [];
	var coordY = [];
  	if(this.coordinates.length){
	  	var coordinate_pairs = this.coordinates.split(";");
	  	this.coordinates = ""; //reset coordinates

	  	for (var i = 0; i < coordinate_pairs.length; i++) {
    		coordX[i] = coordinate_pairs[i].split(",")[0];
	    	coordY[i] = coordinate_pairs[i].split(",")[1];
	    	
		    if (coordX[i] != '' && coordY[i] != '') {	
		    	coordX[i] = parseFloat(coordX[i]) + dx;
		    	coordY[i] = parseFloat(coordY[i]) + dy;
		    	this.storeCoordinates(coordX[i], coordY[i]);
	    	}
	  	}
	}
};

Pencil.prototype.storeCoordinates = function(coordX, coordY) {
	this.coordinates += "" + coordX + "," + coordY + ";";
};



function Rectangle(leftX, leftY, rightX, rightY, fillColor, lineWeight, winWidth){
//function Rectangle(corner1_x, corner1_y, corner2_x, corner2_y, fillColor, lineWeight, winWidth){	
	this.type = "rect";
	this.leftX = leftX;
	this.leftY = leftY;
	this.rightX = rightX;
	this.rightY = rightY;
	this.fillColor = fillColor;
	//this.strokeColor = strokeColor;
	this.lineWeight = lineWeight;
	this.winWidth = winWidth;
	this.dragging = false;
	this.selected = false;

	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Rectangle.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);

	push();
	rectMode(CORNERS);
	stroke(0);
	strokeWeight(this.lineWeight);
	
	if (this.fillColor == 'no-fill') {
		noFill();
	}
	else{
		fill(this.fillColor);	
	}

	scale(scale_ratio);
	rect(this.leftX, this.leftY, this.rightX, this.rightY);

	if (this.selected) {
		push();
		var padding = 5;
		stroke(selectShapeColor);
		strokeWeight(1);
		noFill();
		//Outline
		rectMode(CORNERS);
		rect(this.leftX, this.leftY, this.rightX, this.rightY);
		
		//corners
		fill(selectShapeColor);
		rectMode(CORNER);
		rect(this.left, this.top, padding * 4, padding * 4);
		rect(this.left + this.width - (padding * 4), this.top + this.height - (padding*4), padding * 4, padding * 4);

		//rectMode(CENTER);
		//rect(this.leftX, this.leftY, padding * 4, padding * 4);
		//rect(this.leftX, this.rightY, padding, padding);
		//rect(this.rightX, this.leftY, padding, padding);
		//rect(this.rightX, this.rightY, padding*4, padding*4);	
		pop();
	}
	pop();
};

Rectangle.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	//Find Rectangle based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = this.top * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	console.log("NW");
	    	//originalCornerX = this.leftX;
	    	//originalCornerY = this.leftY;
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	console.log("SE");
	    	//originalCornerX = this.rightX;
	    	//originalCornerY = this.rightY;
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
}

Rectangle.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;

  	this.left = Math.min(this.leftX, this.rightX);
	this.top = Math.min(this.leftY, this.rightY);
	this.width = Math.abs(this.rightX - this.leftX);
	this.height = Math.abs(this.rightY - this.leftY);
}

Rectangle.prototype.resize = function(finalX, finalY) {	
	var scale_ratio = width / this.winWidth;

	switch (cornerUpdated){
		case "NW":
			if (this.leftX < this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			
			break;

		case "SE":
			if (this.leftX > this.rightX) {
				this.leftX = finalX / scale_ratio;
				this.leftY = finalY / scale_ratio;	
			}
			else{
				this.rightX = finalX / scale_ratio;
				this.rightY = finalY / scale_ratio;		
			}
			break;
	}
}


function Write(textString, leftX, leftY, font, fontSize, fillColor, winWidth){
	this.type = "text";
	this.dragging = false;
	this.textString = textString;
	this.leftX = leftX;
	this.leftY = leftY;
	this.font = font;
	this.fontSize = fontSize;
	this.fillColor = fillColor;
	this.winWidth = winWidth;

	this.formattedString = '';
	this.show = true;
	this.textBoxId = Math.random().toString(36).substring(3).toUpperCase();
	this.textBoxWidth = 300;
	this.textBoxHeight = 50;

	this.rightX = leftX + this.textBoxWidth;
  	this.rightY = leftY + this.textBoxHeight;

  	this.left = null;
	this.top = null;
	this.width = null;
	this.height = null;
}

Write.prototype.draw = function() {
	var scale_ratio = width / this.winWidth;

	this.left = this.leftX;
	this.top = this.leftY;
	this.width = this.textBoxWidth;
	this.height = this.textBoxHeight;

	push();
	if (this.show) {
		rectMode(CORNER);
		fill(this.fillColor);
		noStroke();
		textFont(this.font);
		textSize(this.fontSize);
		
		scale(scale_ratio);
		text(this.formattedString, this.left, this.top, this.width);	
		
		if (this.selected) {
			push();
			var padding = 5;
			var cornerLeftX = this.left - padding;
			var cornerLeftY = this.top - padding;
			var cornerRightX = this.left + this.width + padding;
			var cornerRightY = this.top + this.height + padding;

			stroke(selectShapeColor);
			strokeWeight(1);
			noFill();
			//Outline
			rectMode(CORNER);
			//rect(cornerLeftX, cornerLeftY, cornerRightX , cornerRightY);
			rect(this.left - padding, this.top - this.fontSize - padding, this.width + padding , this.height + padding);

			//corners
			fill(selectShapeColor);
			rectMode(CORNER);
			//rect(this.left, this.top, padding * 4, padding * 4);
			rect(this.left + this.width - (padding * 4), this.top + this.height - this.fontSize - (padding*4), padding * 4, padding * 4);

			pop();
		}
	}
	pop();
};


Write.prototype.createTextBox = function(winPosX, winPosY) {
  	this.show = false;
  	
  	var tempTextArea = createElement('textArea');
  	tempTextArea.id(this.textBoxId);
  	tempTextArea.input(updateText);
  	tempTextArea.position(winPosX, winPosY);
    tempTextArea.size(this.textBoxWidth, this.textBoxHeight);
    tempTextArea.value(this.textString);
    tempTextArea.elt.blur();
    tempTextArea.elt.focus();
    tempTextArea.elt.addEventListener("focusout", function(){removeTextBox(tempTextArea.id())});

    tempTextArea.style('background-color', "rgba(255,255,255,0)"); //transparent background
    tempTextArea.style('color', this.fillColor);
    tempTextArea.style('font-size', this.fontSize + "px");
    tempTextArea.style('font-family', this.font);
    tempTextArea.style('border', "1px dashed gray");
};

Write.prototype.resize = function(finalX, finalY) {
	var scale_ratio = width / this.winWidth;

	var new_width = Math.abs(this.leftX - finalX / scale_ratio);

	if (new_width > 200) {
		switch (cornerUpdated){
			case "NW":
				break;

			case "SE":
				break;
		}

		this.textBoxWidth = new_width;
		this.fitToBox(this.textBoxWidth);	
	}	
};

Write.prototype.removeTextBox = function() {
	this.show = true;
	document.getElementById(this.textBoxId).remove();
};

Write.prototype.locate = function(clickX, clickY) {
	var padding = 5;
	
	//Find Write based on click location
	var scale_ratio = width / this.winWidth;

	scaled_left = this.left * scale_ratio;
	scaled_top = (this.top - this.fontSize) * scale_ratio;
	scaled_width = this.width * scale_ratio;
	scaled_height = this.height * scale_ratio;

	if(clickX >= scaled_left && clickX <= (scaled_left + scaled_width) && clickY >= scaled_top && clickY <= (scaled_top + scaled_height)){
		//this.selected = true;

	    //Find NW corner
	    if (clickX >= (scaled_left - scale_ratio*padding*4) && clickX <= (scaled_left + scale_ratio*padding*4) && clickY >= (scaled_top - scale_ratio*padding*4) && clickY <= (scaled_top + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "NW";
	    }
	    //Find SE corner
	    if (clickX >= ((scaled_left+scaled_width) - scale_ratio*padding*4) && clickX <= ((scaled_left+scaled_width) + scale_ratio*padding*4) && clickY >= ((scaled_top+scaled_height) - scale_ratio*padding*4) && clickY <= ((scaled_top+scaled_height) + scale_ratio*padding*4)) {
	    	mode = "resize";
	    	cornerUpdated = "SE";
	    }
	    return true;
	}
	else{
	  	//this.selected = false;
	    return false;
	}
};

Write.prototype.move = function(dx, dy) {
	this.leftX += dx;
  	this.leftY += dy;
  	this.rightX += dx;
  	this.rightY += dy;
};

Write.prototype.fitToBox = function(textBoxWidth) {
	var leftOverText = this.textString;
	var formattedString = '';
  
  	var count = 0;
	var lines = this.textString.split('\n');
	var textBoxHeight = 0;

	for (var j = 0; j < lines.length; j++) {
		if (textWidth(lines[j]) > textBoxWidth) {
			var words = lines[j].split(" ");
			var new_line = "";

			textBoxHeight += (this.fontSize + textLeading());

			for (var i = 0; i < words.length; i++) {
				if (textWidth(new_line + words[i] + " ") < textBoxWidth) {
					new_line += words[i] + " ";
					formattedString += words[i] + " ";
				}
				else {
					formattedString += words[i] + "\n";
					/*var*/ new_line = "";
					textBoxHeight += (this.fontSize + textLeading());
				}
			}
		}
		else {
			formattedString += lines[j] + '\n';
			textBoxHeight += (this.fontSize + textLeading());
		}
	}
	
	textBoxHeight += (this.fontSize + textLeading());

	this.formattedString = formattedString;
	this.textBoxWidth = textBoxWidth;
	this.textBoxHeight = textBoxHeight > 50 ? textBoxHeight : 50;

	this.rightX = this.leftX + this.textBoxWidth;
  	this.rightY = this.leftY + this.textBoxHeight;

  	this.width = this.textBoxWidth;
	this.height = this.textBoxHeight;
};

Write.prototype.edit = function() {
	this.show = false;
	this.createTextBox(this.left, this.top);
}