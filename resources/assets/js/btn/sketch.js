var canvas, shape_option, fs, mode, bgcolor, shapeFillColor, textColor, freeFormColor, selectShapeColor, resizeShapeColor, grid;  

//For Adding Shapes To Canvas
var shapes;
var drag_canvas, new_shape_index, selected_shape, selected_shape_index, startX, startY, originalLeftX, originalLeftY, originalRightX, originalRightY, originalCornerX, originalCornerY, cornerUpdated;

//var newTextArea, resize_corner;
var new_shape, new_text, new_pencil;
var fileButton, fullScreenButton;

var formulaSpan, activeFormulaId, activeMathField;

var zoom, zMin, zMax, zSensitivity, transX, transY;

var window_x_start, window_x_end, window_y_start, window_y_end;
//var shapes_archive, current_shapes_index, undo_steps, redo_steps;

function setup() {
  //frameRate(30);
  initializeBoard();
  createToolbar();  
  sizeSlider = select("#penWeight");
  lineWeightSlider = select("#lineWeight");
  fontSize = select("#fontSize");
  fontStyle = select("#fontStyle");
  formulaFontSize = select("#formulaFontSize");
  formulaFontStyle = select("#formulaFontStyle");
  //createTab(); //create tab and canvas
  canvas = createCanvas(windowWidth, windowHeight);
  var canvas_parent = select("#canvas-region");
  canvas.parent(canvas_parent);
  
  //Canvas only events
  canvas.mouseClicked(function(){
    // if (newTextArea) {
    //   newTextArea.remove();  
    // }
  });
  
  canvas.mouseWheel(function (event){
    if (mouseIsPressed) {
      //scale to zoom in and out
      /*zoom += zSensitivity * event.delta;
      zoom = constrain(zoom, zMin, zMax);
      //uncomment to block page scrolling
      return false;*/
    }
    else{
      //scroll
      if (shapes.length) {
        transX += -event.deltaX;
        transY += -event.deltaY;
        return false;  
      }
    }
  });

  canvas.doubleClicked(function(){
    for (var shape of shapes){
      if ((shape.type == "text" || shape.type == "formula") && shape.locate(mouseX - transX, mouseY - transY)) {
        shape.edit();
      }
    }
  });

  canvas.mousePressed(function(){
    loop();
    deselectAll();

    startX = mouseX - transX;
    startY = mouseY - transY;
    
    switch (mode){
      case "select":
        hidePalletes();
        cursor(HAND);
        findShape();

        //selected_shape.dragging = true;
        //drag_canvas = false; 

        //show toolbar/draw options
        if (selected_shape) {
          switch (selected_shape.type){
            case "text":
            case "formula":
              drawOptions(selected_shape.type);
              break;
            case "ellipse":
            case "rect":
              drawOptions('shape');
              break;
            default:
              drawOptions('freeform');
              break;
          }  
        }
        
        break;

      case "draw":
        new_shape_index = shapes.length;

        switch (shape_option){
          case "pencil":
            new_shape = new Pencil(sizeSlider.value(), freeFormColor, width);    
            //broadcastShape(JSON.stringify(shapes), "update");
            break;
          case "eraser":
            shape_option = "pencil";    
            new_shape = new Pencil(sizeSlider.value(), "white", width);    
            break;
          case "highlighter":
            var highColorRGB = color(freeFormColor);
            var highColor = highColorRGB.toString().replace("1)", "0.3)");
            new_shape = new Pencil(sizeSlider.value(), highColor, width);    
            new_shape.highlighter = true;
            break;
          default:
            /*new_shape = new Rectangle();
            shapes[new_shape_index] = new_shape;
            broadcastShape(JSON.stringify(shapes), "update");*/
            break;
        }

        /*if (shape_option == "pencil") {
          //new_pencil = new Pencil(sizeSlider.value(), freeFormColor, width);    
          new_shape = new Pencil(sizeSlider.value(), freeFormColor, width);    
          broadcastShape(JSON.stringify(shapes), "update");
        }
        if (shape_option == "eraser") {
          shape_option = "pencil";
          //new_pencil = new Pencil(sizeSlider.value(), "white", width);    
          new_shape = new Pencil(sizeSlider.value(), "white", width);    
        }

        if (shape_option == "highlighter") {
          var highColorRGB = color(freeFormColor);
          var highColor = highColorRGB.toString().replace("1)", "0.3)");
          // new_pencil = new Pencil(sizeSlider.value(), highColor, width);    
          // new_pencil.highlighter = true;
          new_shape = new Pencil(sizeSlider.value(), highColor, width);    
          new_shape.highlighter = true;
        }*/
        break;
    }
  });

  canvas.mouseReleased(function(){
    // if (mode == "select"){
    //   cursor(HAND);
      
    //   if (selected_shape){
    //     selected_shape.dragging = false;
    //     selected_shape = selected_shape_index = null;  
    //     startX = startY = null;
    //     originalLeftX = originalLeftY = originalRightX = originalRightY = null;
    //   }
    //   broadcastShape(JSON.stringify(shapes), "update");
    // }

    // if (mode =="draw") {
    //   switch (shape_option){
    //     case "text":
    //       cursor(TEXT);
    //       new_text = new Write('', mouseX - transX, mouseY - transY, fontStyle.elt[fontStyle.elt.selectedIndex].value, Number(fontSize.elt[fontSize.elt.selectedIndex].text), textColor, width);
    //       new_text.createTextBox(mouseX - transX, mouseY - transY);
    //       break;

    //     case "formula":
    //       cursor(TEXT);
    //       new_formula = new Formula('', mouseX - transX, mouseY - transY, formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].text, Number(formulaFontSize.elt[formulaFontSize.elt.selectedIndex].text), formulaColor, width);
    //       new_formula.fontLaTex = formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].value;
    //       new_formula.fontSizeLaTex = formulaFontSize.elt[formulaFontSize.elt.selectedIndex].value;
    //       new_formula.createFormulaBox(/*winMouseX - transX, winMouseY - transY*/);
          
    //       shapes[new_shape_index] = new_formula;
    //       break;
    //     case "pencil":
    //     case "highlighter":
    //       shapes[new_shape_index] = new_shape;
    //       break;
        
    //     default:
    //       new_shape.selected = true;
    //       break;
    //   }
      
    //   broadcastShape(JSON.stringify(shapes), "update");
    // }
    
    // if (mode == "resize"){
    //   mode = "select";
    //   cursor(HAND);
    //   resize_shape = originalCornerX = originalCornerY = cornerUpdated = null;
    //   broadcastShape(JSON.stringify(shapes), "update");
    //   console.log("resize_reset");
    //   console.log("corner: " + cornerUpdated);
    // }

    // if (mode == 'drag'){
    //   cursor(HAND);
    // }

    // noLoop();
  });

  //Draw existing shapes
  if (prior) {
    receiveShapes(JSON.parse(prior));
  }

  if (prior_messages) {
    loadPriorMessages(JSON.parse(prior_messages));
  }
  
  noLoop();
}

function initializeBoard(){
  // shape_option = "pencil";
  // mode = "draw";
  shape_option = null;
  mode = "select";
  fs = false; //full screen
  canvas = null;
  drag_canvas = false;
  //var eraser = null;
  bgcolor = '#E0E0E0';
  //var fillColor = "#2a9bbe";
  shapeFillColor = "white";
  shapeStrokeColor = "black";
  textColor = "black";
  formulaColor = "Black";
  freeFormColor = "black";

  selectShapeColor = "#964FE8"; //when shapes are located and selected
  resizeShapeColor = "#660066";
  grid = true; 

  shapes = [];
  new_shape = new_text = new_pencil = null;
  new_shape_index = selected_shape = selected_shape_index = startX = startY = originalLeftX = originalLeftY = originalRightX = originalRightY = originalCornerX = originalCornerY = cornerUpdated = null;

  formulaSpan = activeFormulaId = null; 
  activeMathField = null;

  zoom = 1.00;
  zMin = 0.8;
  zMax = 5.00;
  zSensitivity = 0.005;

  transX = transY = 0;
  //shapes_archive = [];
  //current_shapes_index = undo_steps = redo_steps = 0;
}

function resetSketch(){
  var decision = confirm('Are you sure you want to erase the board? Consider saving the board first by clicking the  download arrow button');
  if (decision) {
    initializeBoard();
    redraw();
    broadcastShape(JSON.stringify(shapes), "erase-board");
  }
}

function draw() {
  background(255);
  
  scale(zoom);

  if (grid) {
    stroke(230);
    //Horizontal lines
    for (var i = 0; i <= height / zoom; i+=10) {
      
      if (i % 50 == 0){
        strokeWeight(2);
        
      }
      else{
        strokeWeight(1);
      }
      line(0, i, width / zoom, i);
    }
    
    //Vertical lines
    for (var i = 0; i <= width / zoom; i+=10) {
      if (i % 50 == 0){
        strokeWeight(2);
        
      }
      else{
        strokeWeight(1);
      }
      line(i, 0, i, height / zoom);
    }
  }

  if (keyIsPressed) {
    moveShape(false);
  }

  translate(transX, transY);
  
  window_x_start = window_y_start = 99999999;
  window_x_end = window_y_end = 0;

  for (var shape of shapes){
    if (shape.type) {
      shape.draw();  
      window_x_start = Math.min(window_x_start, shape.left);
      window_x_end = Math.max(window_x_end, shape.left + shape.width);
      window_y_start = Math.min(window_y_start, shape.top);
      window_y_end = Math.max(window_y_end, shape.top + shape.height);
    }
  }
  showCanvasArrows();
  textSize(24);
  text(frameCount, 80 - transX, height - 200 - transY);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function showCanvasArrows(){
  var arrows = document.getElementsByClassName('canvas-arrows');
  
  //Left Arrows
  if (window_x_start + transX < 0) {
    document.getElementById('more-left').style.visibility = "visible";  
  }
  else{
    document.getElementById('more-left').style.visibility = "hidden";  
  }

  //Right Arrows
  if (window_x_end + transX > width) {
    document.getElementById('more-right').style.visibility = "visible";  
  }
  else{
    document.getElementById('more-right').style.visibility = "hidden";  
  }
  
  //Top Arrows
  if (window_y_start + transY < 0) {
    document.getElementById('more-top').style.visibility = "visible";  
  }
  else{
    document.getElementById('more-top').style.visibility = "hidden";  
  }

  //Bottom Arrows
  if (window_y_end + transY > height) {
    document.getElementById('more-bottom').style.visibility = "visible";  
  }
  else{
    document.getElementById('more-bottom').style.visibility = "hidden";  
  }
}

function mouseDragged() {
    
    switch (mode){
      case "select":
        loop();
        updateShapes();
        break;

      case "draw":
        if (new_shape_index != null) {
          loop();
          updateShapes();    
        }
        break;

      case "drag":
        loop();
        dragCanvas();
        break;
      
      case "resize":
        findShape();
        if (selected_shape && cornerUpdated) {
          cursor(MOVE);
          selected_shape.resize(mouseX - transX, mouseY - transY);
          broadcastShape(JSON.stringify(shapes), "update");  
        }
        break;
    }  
  return false;
}

function mouseReleased(){
  //noLoop();
  if (mode == "select"){
      cursor(HAND);
      
      if (selected_shape){
        selected_shape.dragging = false;
        selected_shape = selected_shape_index = null;  
        startX = startY = null;
        originalLeftX = originalLeftY = originalRightX = originalRightY = null;
      }
      broadcastShape(JSON.stringify(shapes), "update");
    }

    if (mode =="draw") {
      switch (shape_option){
        case "text":
          cursor(TEXT);
          new_text = new Write('', mouseX - transX, mouseY - transY, fontStyle.elt[fontStyle.elt.selectedIndex].value, Number(fontSize.elt[fontSize.elt.selectedIndex].text), textColor, width);
          new_text.createTextBox(mouseX - transX, mouseY - transY);
          break;

        case "formula":
          cursor(TEXT);
          new_formula = new Formula('', mouseX - transX, mouseY - transY, formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].text, Number(formulaFontSize.elt[formulaFontSize.elt.selectedIndex].text), formulaColor, width);
          new_formula.fontLaTex = formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].value;
          new_formula.fontSizeLaTex = formulaFontSize.elt[formulaFontSize.elt.selectedIndex].value;
          new_formula.createFormulaBox(/*winMouseX - transX, winMouseY - transY*/);
          
          shapes[new_shape_index] = new_formula;
          break;
        case "pencil":
        case "highlighter":
          if (new_shape && new_shape_index != null) {
            shapes[new_shape_index] = new_shape;
            new_shape_index = null;  
            broadcastShape(JSON.stringify(shapes)/*, "update"*/);
          }
          break;
        
        default:
          if (new_shape && new_shape_index != null) {
            broadcastShape(JSON.stringify(shapes)/*, "update"*/);
            new_shape.selected = true;
            new_shape_index = null;  
          }
          break;
      }
      
      //broadcastShape(JSON.stringify(shapes), "update");
    }
    
    if (mode == "resize"){
      mode = "select";
      cursor(HAND);
      selected_shape = selected_shape_index = null;  
      originalCornerX = originalCornerY = cornerUpdated = null;
      broadcastShape(JSON.stringify(shapes), "update");
    }

    if (mode == 'drag'){
      cursor(HAND);
    }

    noLoop();
}

function mouseWheel(event){
}

function mouseMoved(){
  if (mode == "select") {
    
    for(var shape of shapes){
      if (shape.locate(mouseX - transX, mouseY - transY)) {
        cursor(HAND);
        break;
      }
      else{
        cursor(ARROW);
      }
    }
  }
}

function keyPressed(){
  //Updating Selected Shape
  moveShape(true);
/*  for (var shape of shapes){
    if (shape.selected) {
      switch (keyCode){
        case UP_ARROW:
          loop();
          shape.move(0,-5);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case DOWN_ARROW:
          loop();
          shape.move(0,5);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case LEFT_ARROW:
          loop();
          shape.move(-5,0);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case RIGHT_ARROW:
          loop();
          shape.move(5,0);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case DELETE:
          loop();
          deleteShape();
          break;
      }
    }
  }*/
  //shortcuts
  switch (keyCode){
    case DELETE:
      for (var shape of shapes)
      {
        if (shape.selected) {
          loop();
          deleteShape();
        }
      }
      break;

    case ESCAPE:
      mode = "select";
      cursor(HAND);
      deselectAll();

      //get out of full screen
      fs = true;
      toggleFullScreen();
      break; 

    case 122:
      //F11 = fullscreen
      toggleFullScreen();
      return false;
      break;
  }

  //Multipe Key Shortcuts
  if(keyIsDown(CONTROL)){
    switch  (key){
      case "Z":
        undo();
        return false; //prevent default browser behavior
        break;
      case "Y":
        redo();
        return false; //prevent default browser behavior
        break;
      //Clear Canvas / New Canvas
      //Duplicate selected shape
      //Delete selected Shape
      //Save One Board, save all boards
      //Select All Shapes
      //Ellipse, rect, etc
    }
  }
}

function keyReleased(){
  noLoop();
}

function changeMode(selected_color){
  mode = selected_color;
  drag_canvas = false;

  switch (mode){
    /*case "select":
      break;*/
    case "drag":
      cursor(HAND);
      break;
    case "draw":
      cursor(CROSS);
      break;
    case "select":
      cursor(ARROW);
      break;
    case "resize":
      cursor(MOVE);
      break;
    default:
      cursor(ARROW);
      break;
  }
}

function selectColor(selected_color, shape_type){
  switch (shape_type){
    case "freeform":
      freeFormColor = selected_color;
      break;

    case "shape":
      shapeFillColor = selected_color;
      break;

    case "text":
      textColor = selected_color;
      break;  
    case "formula":
      formulaColor = selected_color;
      break;
  }

  for(var shape of shapes){
    if (shape.selected == true) {
      shape.fillColor = selected_color;
      shape.strokeColor = selected_color;
      
      broadcastShape(JSON.stringify(shapes), "update");

      if (shape.type =="formula") {
        shape.createFormulaImage();
        var shape_index = shapes.indexOf(shape);
        shapes.splice(shape_index, 1);
        shapes.push(shape);
        broadcastShape(JSON.stringify(shapes), "formula");
      }

      redraw();
      break;
    }
  }
}

function updateFont(option, type){
  switch (option){
    case "size":
      for(var shape of shapes){
        if (shape.selected == true) {          
          if (shape.type == "text") {
            shape.fontSize = Number(fontSize.elt[fontSize.elt.selectedIndex].text);
            shape.fitToBox(shape.textBoxWidth);
            broadcastShape(JSON.stringify(shapes), "update");
            redraw();
          }
          else if (shape.type == "formula"){
            shape.fontSize = Number(formulaFontSize.elt[formulaFontSize.elt.selectedIndex].text);
            shape.fontSizeLaTex = formulaFontSize.elt[formulaFontSize.elt.selectedIndex].value;
            shape.createFormulaImage();

            broadcastShape(JSON.stringify(shapes), "update");
            var shape_index = shapes.indexOf(shape);
            shapes.splice(shape_index, 1);
            shapes.push(shape);
            broadcastShape(JSON.stringify(shapes), "formula");
            redraw();
          }
          break;
        }
      }
      break;
      
    case "style":
      for(var shape of shapes){
        if (shape.selected == true) {
          if (shape.type == "text") {
            shape.font = fontStyle.elt[fontStyle.elt.selectedIndex].value;
            shape.fitToBox(shape.textBoxWidth);
            broadcastShape(JSON.stringify(shapes), "update");
            redraw();
          }
          else if (shape.type == "formula"){
            shape.font = formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].text;
            shape.fontLaTex = formulaFontStyle.elt[formulaFontStyle.elt.selectedIndex].value;
            shape.createFormulaImage();

            broadcastShape(JSON.stringify(shapes), "update");
            var shape_index = shapes.indexOf(shape);
            shapes.splice(shape_index, 1);
            shapes.push(shape);
            broadcastShape(JSON.stringify(shapes), "formula");

            redraw();
          }
         break;
        }
      }
      break;
  }
}
function selectLineWeight(lineWeight){
  for(var shape of shapes){
    if (shape.selected == true && shape.lineWeight) {
      shape.lineWeight = lineWeight;
      broadcastShape(JSON.stringify(shapes), "update");
      redraw();
      break;
    }
  }
}

function undo(){
  broadcastShape(JSON.stringify(shapes), 'undo');
}

function redo(){
  broadcastShape(JSON.stringify(shapes), 'redo');
}

function moveShape(redraw){
  for (var shape of shapes){
    if (shape.selected) {
      switch (keyCode){
        case UP_ARROW:
          if (redraw) {
            loop();
          }
          shape.move(0,-5);
          broadcastShape(JSON.stringify(shapes), "update");

          return false;
          break;
        case DOWN_ARROW:
          if (redraw) {
            loop();
          }
          shape.move(0,5);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case LEFT_ARROW:
          if (redraw) {
            loop();
          }
          shape.move(-5,0);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
        case RIGHT_ARROW:
          if (redraw) {
            loop();
          }
          shape.move(5,0);
          broadcastShape(JSON.stringify(shapes), "update");
          return false;
          break;
      }
    }
  }
}

function showShapeOptions(shape_option_id){
  var menus = document.getElementsByClassName("shapes-menu");
  for(var i = 0; i < menus.length; i++)
  {
     menus[i].style.display = 'none';
  }
  document.getElementById(shape_option_id).style.display = "inline";
}

function uploadFile(file){
  if (file.type == 'image') {
    loop();
    var count = 0;
    var img = createImg(file.data);
    var img_width = img.width;
    var img_height = img.height;
    img.hide();

    if (img_width > width) {
      //scale image to canvas
      var new_width = map(img_width, 0, img_width, 0, width);
      var new_height = map(img_height, 0, img_height, 0, height);
      img.size(new_width, new_height);
    }

    new_shape = new Image(img, 80, 30, img_width, img_height, width);
    new_shape.src_base64 = img.elt.src;

    shapes[shapes.length] = new_shape;
    
    new_shape.selected = true;
    mode = "select";
    broadcastShape(JSON.stringify(shapes), 'update');
    console.log("broadcasted");
    noLoop();
    //redraw();
  }
}

function saveBoard(){
  var imgCanvasWidthMax = 810; //2550px = 300dpi
  var imgCanvasHeightMax = 1048; //3300px = 300dpi
  var portrait_ratio = 8.5 / 11;
  var landscape_ratio = 1 / portrait_ratio;

  var pagePadding = 48; //150px = 0.5"
  var shapeMinX = shapeMinY = shapeMaxX = shapeMaxY = 0;
  var dx, dy, drawingWidth, drawingHeight, drawing_ratio, orientation;
  var scale_width = scale_height = 1;
  var original_zoom = zoom;
  var original_transX = transX;
  var original_transY = transY;

  for(var shape of shapes){
    shape.selected = false;

    shapeMinX = Math.min(shapeMinX, shape.leftX, shape.rightX);
    shapeMinY = Math.min(shapeMinY, shape.leftY, shape.rightY);
    shapeMaxX = Math.max(shapeMaxX, shape.leftX, shape.rightX);
    shapeMaxY = Math.max(shapeMaxY, shape.leftY, shape.rightY);
  }
  
  //calculate delta to move minX and minY to padding coordinate
  dx = pagePadding - window_x_start;
  dy = pagePadding - window_y_start;
  drawingWidth = window_x_end - window_x_start;
  drawingHeight = window_y_end - window_y_start;

  drawing_ratio = drawingWidth / drawingHeight;

  zoom = 1;
  transX = transY = 0;
  for(var shape of shapes){
    if (shape.type) {
      shape.move(dx, dy);
    }
  }

  if (Math.abs(portrait_ratio - drawing_ratio) < Math.abs(landscape_ratio - drawing_ratio)) {
    orientation = "portrait";
    drawingWidth = imgCanvasWidthMax - (pagePadding * 2);
    drawingHeight = imgCanvasHeightMax - (pagePadding * 2);
  }
  else{
    orientation = "landscape";
    drawingWidth = imgCanvasHeightMax - (pagePadding * 2);
    drawingHeight = imgCanvasWidthMax - (pagePadding * 2);  
  }
  
  resizeCanvas(drawingWidth, drawingHeight);

  redraw();
  var credit = "Courtesy of BuffTutor Online";
  var font_size = 12;
  push();
  fill(100);
  noStroke();
  textSize(font_size);
  textFont('Georgia');

  text(credit, drawingWidth - textWidth(credit) - font_size, drawingHeight - font_size);
  pop();

  html2canvas(document.getElementById('canvas-region'), {
      allowTaint: true,
      useCORS: true,
      onrendered: function(canvas_snapshot){
        var temp_img = canvas_snapshot.toDataURL('image/png');
        var doc = new jsPDF(orientation, "in", "letter");
        //DEFINITION: jsPDF(orientation, unit, format, compress);
        doc.addImage(temp_img, 'JPEG', 0.5, 0.5);
        doc.save('test_board.pdf');
      }
  });
  
  //move all shapes back
  for(var shape of shapes){
    shape.move(-dx, -dy);
  }

  resizeCanvas(windowWidth, windowHeight);
  zoom = original_zoom;
  transX = original_transX;
  tranY = original_transY;
  redraw();
}