function createToolbar(){
  fileButton = createFileInput(uploadFile);
  fileButton.id('file-upload');
  fileButton.style('display:none');
  fileButton.attribute("accept", "image/x-png,image/gif,image/jpeg");

  //createP('Math Options');
  var math_button_width = "35px";
  var math_symbols_div = select("#math-symbols");
  var greek_letters_div = select("#greek-letters");
  var capital_greek_letters_div = select("#capital-greek-letters");
  var calculus_div = select("#calculus");
  var arrows_div = select("#arrows");
  var logic_div = select("#logic");

  var math_options = [{label: "&#188;", latex: "frac"},
                        {label: "&#8730;", latex: "sqrt"},
                        /*{label: "&#8731;", latex: "sqrt[]{}"},*/
                        {label: "&#177;", latex: "pm"},
                        {label: "&#215;", latex: "times"},
                        {label: "&#42;", latex: "ast"},
                        {label: "&#247;", latex: "div"},
                        {label: "&#8804;", latex: "leq"},
                        {label: "&#8805;", latex: "geq"},
                        {label: "&#8800;", latex: "neq"},
                        {label: "&equiv;", latex: "equiv"},
                        {label: "&asymp;", latex: "approx"},
                        {label: "&#8734;", latex: "infty"},
                        {label: "&#8226;", latex: "cdot"},
                        {label: "&#9900;", latex: "circ"},
                        {label: "&oplus;", latex: "oplus"},
                        {label: "&otimes;", latex: "otimes"},
                        {label: "&#8857;", latex: "odot"},
                        {label: "&#8861;", latex: "circledash"},
                        ];
  var temp_button;
  for (var i = 0; i < math_options.length; i++) {
    temp_button = createButton(math_options[i].label);
    temp_button.value(math_options[i].latex);
    temp_button.style("width", math_button_width);
    temp_button.mouseClicked(insertMathSymbol);
    temp_button.parent(math_symbols_div);
  } 

  var arrow_options = [{label: "&rarr;", latex: "rightarrow"},
                        {label: "&larr;", latex: "leftarrow"},
                        {label: "&uarr;", latex: "uparrow"},
                        {label: "&darr;", latex: "downarrow"},
                        {label: "&harr;", latex: "leftrightarrow"},
                        /*{label: "&#8644;", latex: "rightleftarrows"},
                        {label: "&#8646;", latex: "leftrightarrows"},
                        {label: "&#8652;", latex: "rightleftharpoons"},
                        {label: "&#8651;", latex: "leftrightharpoons"},*/
                        {label: "&#8640;", latex: "rightharpoonup"},
                        {label: "&#8636;", latex: "leftharpoonup"},
                        {label: "&hArr;", latex: "Leftrightarrow"},
                        {label: "&rArr;", latex: "Rightarrow"},
                        {label: "&lArr;", latex: "Leftarrow"},
                        {label: "&uArr;", latex: "Uparrow"},
                        {label: "&dArr;", latex: "Downarrow"},
                        ];
  for (var i = 0; i < arrow_options.length; i++) {
    temp_button = createButton(arrow_options[i].label);
    temp_button.value(arrow_options[i].latex);
    temp_button.style("width", math_button_width);
    temp_button.mouseClicked(insertMathSymbol);
    temp_button.parent(arrows_div);
  } 

  var calculus_options = [{label: "&#8747;", latex: "int"},
                          {label: "&part;", latex: "partial"},
                          {label: "&nabla;", latex: "nabla"},
                          {label: "&#8750;", latex: "oint"},
                          {label: "&#8721;", latex: "sum"},
                          ];
  for (var i = 0; i < calculus_options.length; i++) {
    temp_button = createButton(calculus_options[i].label);
    temp_button.value(calculus_options[i].latex);
    temp_button.style("width", math_button_width);
    temp_button.mouseClicked(insertMathSymbol);
    temp_button.parent(calculus_div);
  } 

  var greek_letters = ["alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta", "kappa", "lambda", "mu", "nu", "xi", "pi", "rho", "sigma","tau", "upsilon", "phi", "chi", "psi", "omega"];

  for (var i = 0; i < greek_letters.length; i++) {
    temp_button = createButton("&" + greek_letters[i] + ";");
    temp_button.value(greek_letters[i]);
    temp_button.style("width", math_button_width);
    temp_button.mouseClicked(insertMathSymbol);
    temp_button.parent(greek_letters_div);

    var skipped_letters = ["alpha", "beta", "epsilon", "zeta", "eta", "kappa", "mu", "nu", "rho", "tau", "upsilon", "chi"];

    if (!(skipped_letters.includes(greek_letters[i]))) {
      var greek_uppercase = greek_letters[i].charAt(0).toUpperCase() + greek_letters[i].slice(1);
      temp_button = createButton("&" + greek_uppercase + ";");
      temp_button.value(greek_uppercase);
      temp_button.style("width", math_button_width);
      temp_button.mouseClicked(insertMathSymbol);
      temp_button.parent(capital_greek_letters_div);  
    }
  }
    
  var logic_options = [{label: "&cup;", latex: "cup"},
                        {label: "&cap;", latex: "cap"},
                        {label: "&#8709;", latex: "varnothing"},
                        {label: "&#8834;", latex: "subset"},
                        {label: "&#8835;", latex: "supset"},
                        {label: "&#8741;", latex: "parallel"},
                        {label: "&#8742;", latex: "nparallel"},
                        {label: "&isin;", latex: "in"},
                        {label: "&ni;", latex: "ni"},];

  for (var i = 0; i < logic_options.length; i++) {
    temp_button = createButton(logic_options[i].label);
    temp_button.value(logic_options[i].latex);
    temp_button.style("width", math_button_width);
    temp_button.mouseClicked(insertMathSymbol);
    temp_button.parent(logic_div);
  } 
}

function toggleFullScreen(){
  var button_image_size = "20px";

  fs = !fs;
  if (fs == true) {
    //document.getElementById('toggle-screen').innerHTML = "<img src='images/symbols/minimize.svg' style='height:" + button_image_size + ";padding:0'>";
    document.getElementById('toggle-screen').innerHTML = "<img src='" + baseUrl + "images/symbols/minimize.svg'>";
  }
  else{
    //document.getElementById('toggle-screen').innerHTML = "<img src='images/symbols/expand.svg' style='height:" + button_image_size + ";padding:0'>";
    document.getElementById('toggle-screen').innerHTML = "<img src='" + baseUrl + "images/symbols/expand.svg'>";
  }
  fullscreen(fs);
}

function drawOptions(type){
  hidePalletes();
  
  var pallete_div = document.getElementById(type +'-pallete');
  if (pallete_div) {
    pallete_div.style.display = "block";
  }
}

function hidePalletes(){
  var palletes = document.getElementsByClassName('pallete');

  for (var pallete of palletes){
    pallete.style.display = "none";
  }
}

