<!DOCTYPE html>
<html lang="en">
<head>

	<title id="title">{{$event_channel}} Room</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="js/app.js"></script>
    <script>
            Echo.channel("{{$event_channel}}")
            .listen('ShapeBroadcasted', (e) =>{
                receiveShapes(JSON.parse(e.data));
            });
    </script> 
    {{--For Ajax & Notifications--}}
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        function broadcastShape(shapes){
            var headers = null;

            if (Echo && Echo.socketId()) {
                headers = {'X-SOCKET-ID': Echo.socketId()};
            }

            $.ajax({
                type: 'POST',
                headers: headers,
                url: "{{route('broadcast.shape')}}",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'), 
                    shapes: shapes,
                    event_channel: "{{$event_channel}}"
                },
                success: function(data){
                    //console.log(data.channel);
                    //console.log(data.shapes);
                }
            });     
        }  
    </script>
	
	{{--P5JS--}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/p5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/addons/p5.dom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/addons/p5.sound.js"></script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap-reboot.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="template/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="template/css/fonts.min.css">

	<link rel="stylesheet" type="text/css" href="css/whiteboard.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="{{asset('js/whiteboard.js')}}"></script>
	<script>
        var MQ = MathQuill.getInterface(2);
    </script>
    
	{{--For Tabs--}}
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/main.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/fonts.min.css')}}">

	<!-- Main Font -->
	<script src="{{asset('template/js/webfontloader.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	{{-- <style> body {padding: 0; margin: 0;} canvas {vertical-align: top;} </style> --}}

	
	{{--TWILIO--}}
	<script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
    {{--Must come after CDN / twilio-video.js --}}
    {{-- <script src="{{ URL::asset('js/twilio.js') }}"></script> --}}
    <script src="{{asset('js/tw-va.js')}}"></script>
    <script>
        Video.createLocalTracks({
           /*audio: true,
           video: { width: 300 }*/
           audio: false,
           video: false
        }).then(function(localTracks) {
           return Video.connect('{{ $accessToken }}', {
               name: '{{ $roomName }}',
               tracks: localTracks,
               video: { width: 300 }
           });
        }).then(function(room) {
           console.log('Successfully joined a Room: ', room.name);
        
           room.participants.forEach(participantConnected);
        
           var previewContainer = document.getElementById(room.localParticipant.sid);
           if (!previewContainer || !previewContainer.querySelector('video')) {
               participantConnected(room.localParticipant);
           }
        
           room.on('participantConnected', function(participant) {
               console.log("Joining: '"  + participant.identity +  "'");
               participantConnected(participant);
           });
        
           room.on('participantDisconnected', function(participant) {
               console.log("Disconnected: '" +  participant.identity +  "'");
               participantDisconnected(participant);
           });
        }, function(error){
            console.log('Unable to connect to Room: ' + error.message);
        });
    </script>
</head>
<body>
	<!-- Fixed Sidebar Left -->

	<div class="btn btn-control bg-blue more" style="position:fixed;left:5%;bottom:10%; z-index: 100;">
		<b style="color:white">&#129095;<br></b>
		{{-- <b style="color:white">&#8691;<br></b> --}}

		<ul class="more-dropdown more-with-triangle triangle-bottom-right" style="text-align:right">
			<li>
				<a href="#" data-toggle="modal" onclick="document.getElementById('file-upload').click()">Upload image <br><small>(jpeg/png)</small></a>
			</li>
			<li>
				<a href="#" onclick="saveBoard()" data-toggle="modal">Save as jpeg</a>
			</li>
			
		</ul>
	</div>
	<!-- End Sidebar Left -->

{{-- 	<!-- Fixed Sidebar Right -->

	@include('layouts.whiteboard.sidebar_right')

	<!-- ... end Fixed Sidebar Right --> --}}


	<!-- Header-BP -->

	@include('layouts.whiteboard.header')

	<!-- ... end Header-BP -->

	{{-- <div class="header-spacer"></div> --}}

	<div id="main-content">
		<div class="row">
			<div id="canvas-region">
			
			</div>

			<!--<div style="z-index:10;position:absolute; right:0;top:0;">
				<div class="ui-block popup-chat" style="background-color: black; min-height:230px">
					<div class="ui-block-content"  id="media-div" style="padding:0px 10px 5px">
						{{-- <div id="media-div">
	    				
	    				</div> --}}
					</div>
				</div>
			</div>-->
			{{-- <div style="z-index:9;position:absolute; right:0;bottom:0;">		
				@include('layouts.whiteboard.chat_popup')
			</div>			 --}}
		</div>
	</div>	
	<div class="fixed-sidebar right">
		<div style="z-index:10;position:absolute; right:0;top:0;">
			<div class="ui-block popup-chat" style="background-color: black; min-height:230px">
				<div class="ui-block-content"  id="media-div" style="padding:0px 10px 5px">
				</div>
			</div>
		</div>
	</div>

	{{-- <!-- Window-popup-CHAT for responsive min-width: 768px -->

	@include('layouts.whiteboard.chat_popup_responsive')

	<!-- ... end Window-popup-CHAT for responsive min-width: 768px --> --}}

	
	

	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    
    <script>
      var socket = io.connect(window.location.hostname + ":6001");
    </script>
    
    {{--Main JS --}}
	<script src="{{asset('js/whiteboard.js')}}"></script>
	<script src="{{asset('js/app.js')}}"></script>

    
	<!-- JS Scripts -->
	<script src="{{asset('template/js/jquery-3.2.1.js')}}"></script>
	<script src="{{asset('template/js/jquery.appear.js')}}"></script>
	<script src="{{asset('template/js/jquery.mousewheel.js')}}"></script>
	<script src="{{asset('template/js/perfect-scrollbar.js')}}"></script>
	<script src="{{asset('template/js/jquery.matchHeight.js')}}"></script>
	<script src="{{asset('template/js/svgxuse.js')}}"></script>
	<script src="{{asset('template/js/imagesloaded.pkgd.js')}}"></script>
	<script src="{{asset('template/js/Headroom.js')}}"></script>
	<script src="{{asset('template/js/velocity.js')}}"></script>
	<script src="{{asset('template/js/ScrollMagic.js')}}"></script>
	<script src="{{asset('template/js/jquery.waypoints.js')}}"></script>
	<script src="{{asset('template/js/jquery.countTo.js')}}"></script>
	<script src="{{asset('template/js/popper.min.js')}}"></script>
	<script src="{{asset('template/js/material.min.js')}}"></script>
	<script src="{{asset('template/js/bootstrap-select.js')}}"></script>
	<script src="{{asset('template/js/smooth-scroll.js')}}"></script>
	<script src="{{asset('template/js/selectize.js')}}"></script>
	<script src="{{asset('template/js/swiper.jquery.js')}}"></script>
	<script src="{{asset('template/js/moment.js')}}"></script>
	<script src="{{asset('template/js/isotope.pkgd.js')}}"></script>
	<script src="{{asset('template/js/loader.js')}}"></script>
	<script src="{{asset('template/js/jquery.magnific-popup.js')}}"></script>
	<script src="{{asset('template/js/sticky-sidebar.js')}}"></script>

	<script src="{{asset('template/js/base-init.js')}}"></script>
	
	<script defer src="{{asset('template/fonts/fontawesome-all.js')}}"></script>
	<script src="{{asset('template/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
</body>
</html>