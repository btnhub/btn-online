<header class="header" id="site-header">

	<div class="page-title">
		<h6 id="page-title"><a href="https://bufftutor.com" target="_blank" style="text-decoration: none;color:white"><img src="{{asset('images/BuffTutor-Logo-Small.png')}}" alt="BuffTutor-Logo" width="50" style="float:left;margin-right:10px">
		BuffTutor <br>Online <small>BETA</small></h6></a>

	</div>

	<div class="header-content-wrapper">
		<div class="control-block"{{--  style="float:left" --}}>			
			<div class="author-page author vcard inline-items more">
				<a href="#" class="author-name fn">
					<div class="author-title">
						James Spiegel <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon')}}"></use></svg>
					</div>
					{{-- <span class="author-subtitle">Student</span> --}}
				</a>
				<div class="author-thumb">
					{{-- <img alt="author" src="img/author-page.jpg" class="avatar">
					<span class="icon-status online"></span> --}}
					<div class="more-dropdown more-with-triangle">
						<div class="mCustomScrollbar" data-mcs-theme="dark">
							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Your Account</h6>
							</div>

							<ul class="account-settings">
								<li>
									<a href="#">

										<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-happy-face-icon')}}"></use></svg>

										<span>My Profile</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-star-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Rate Tutor"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-star-icon')}}"></use></svg>

										<span>Rate My Tutor</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-computer-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Charge Session"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-computer-icon')}}"></use></svg>

										<span>Charge Session <br>
										<small>AT END OF SESSION!</small>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-logout-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-logout-icon')}}"></use></svg>

										<span>Leave Room</span>
									</a>
								</li>
							</ul>
						</div>

					</div>
				</div>
				

			</div>
		</div>

	</div>

</header>
<!-- Responsive Header-BP -->

<header class="header header-responsive" id="site-header-responsive" style="padding-right:10px;padding-left:50px">

	<div class="header-content-wrapper">
		<ul class="nav nav-tabs mobile-app-tabs" role="tablist">
			{{-- <li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#freeform" role="tab" onClick="showShapeOptions('freeform-options');">
					<div class="control-icon has-items">
						<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-happy-face-icon')}}"></use></svg>
						<div id="freeform-label-mobile" class="label-avatar" style="background-color: black"></div>

					</div>
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#shapes" role="tab" onClick="showShapeOptions('shapes-options');">
					<div class="control-icon has-items">
						<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')}}"></use></svg>
						<div id="shapes-label-mobile" class="label-avatar" style="background-color: white"></div>
					</div>
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#text" role="tab" onClick="showShapeOptions('text-options');">
					<div class="control-icon has-items">
						<svg class="olymp-thunder-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-thunder-icon')}}"></use></svg>
						<div id="text-label-mobile" class="label-avatar" style="background-color: black"></div>
					</div>
				</a>
			</li> --}}

			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#shapes" role="tab" onClick="toggleChat();">
					<div class="control-icon has-items">
						<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')}}"></use></svg>
					</div>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#erase" role="tab">
					<div class="control-icon has-items">
						James Spiegel <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon')}}"></use></svg>
					</div>
				</a>
			</li>
			{{-- <li style="padding-right:10px">
				<a href="#" onclick="changeMode('drag')" style="text-decoration:none">Drag</a>
			</li> --}}
			{{-- <li style="padding-right:10px">
				<a href="#" onclick="toggleFullScreen()" data-toggle="tooltip" data-placement="right" data-original-title="FULLSCREEN" style="text-decoration:none">
					<b style="font-size:20px;">&#10530;</b>
				</a>
			</li> --}}
		</ul>
	</div>

	<!-- Tab panes -->
	<div class="tab-content tab-content-responsive">

		{{-- <div class="tab-pane " id="freeform" role="tabpanel">

			<div class="mCustomScrollbar" data-mcs-theme="dark">
				<ul class="notification-list friend-requests">
					<li>
						<div class="notification-event">
							@include('layouts.whiteboard.partials.freeform_options')
							<div>
								@include('layouts.whiteboard.partials.color_pallete', ['label_id' => "freeform-label", 'shape_type' => 'freeform'])
							</div>
						</div>
					</li>
				</ul>
			</div>

		</div>

		<div class="tab-pane " id="shapes" role="tabpanel">

			<div class="mCustomScrollbar" data-mcs-theme="dark">

				<ul class="notification-list chat-message">
					<li class="message-unread" style="padding:10px;">
						<div class="notification-event">
							@include('layouts.whiteboard.partials.shapes_options')
							<div>
								@include('layouts.whiteboard.partials.color_pallete', ['label_id' => "shapes-label", 'shape_type' => 'shape'])
								<b><a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="TRANSPARENT" style="color:black;font-size:20px;text-decoration:none;display:inline;padding:17px;" onclick="selectColor('no-fill', 'shape');labelColor('shapes-label', '')">&#8416;</a></b>
							</div>
						</div>
					</li>
				</ul>
			</div>

		</div>

		<div class="tab-pane " id="text" role="tabpanel">

			<div class="mCustomScrollbar" data-mcs-theme="dark">
				<ul class="notification-list">
					<li>
						<div class="notification-event">
							@include('layouts.whiteboard.partials.text_options')
							<div>
								@include('layouts.whiteboard.partials.color_pallete', ['label_id' => "text-label", 'shape_type' => 'text'])
							</div>
						</div>
					</li>
				</ul>
			</div>

		</div> --}}

		<div class="tab-pane " id="erase" role="tabpanel">

			<div class="mCustomScrollbar" data-mcs-theme="dark">
				<ul class="notification-list">
					<li>
						<div class="notification-event">
							<ul class="account-settings">
								<li>
									<a href="#">

										<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-happy-face-icon')}}"></use></svg>

										<span>My Profile</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-star-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Rate Tutor"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-star-icon')}}"></use></svg>

										<span>Rate My Tutor</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-computer-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Charge Session"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-computer-icon')}}"></use></svg>

										<span>Charge Session <br>
										<small>AT END OF SESSION!</small>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<svg class="olymp-logout-icon"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-logout-icon')}}"></use></svg>

										<span>Leave Room</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- ... end  Tab panes -->

</header>

<!-- ... end Responsive Header-BP -->