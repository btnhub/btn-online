<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="FREEFORM" style="text-decoration:none;font-size:20px;padding:5px;display:inline;" onclick="selectShape('pencil')">
	<b>&#128393;</b>
</a>
<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="ERASER" style="text-decoration:none;font-size:20px;padding:5px;display:inline;" onclick="selectShape('eraser')">
	<b>&#9003;</b>
</a>
<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="HIGHLIGHTER" style="text-decoration:none;font-size:20px;padding:5px;display:inline;" onclick="selectShape('highlighter')">
	<b>&#128393;</b>
</a>	
<br>
<b>Size</b>
<input type="range" min="2" max="30" value="5" class="slider" id="penWeight">	