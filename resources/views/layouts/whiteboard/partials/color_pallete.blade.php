<?php $colors = ['black', 'white', 'grey', 'red', 'orange', 'yellow', 'green', 'blue', 'purple', 'magenta'];?>

@foreach ($colors as $color)
	{{-- <a href="#" style="color:{{$color}};font-size:20px;text-decoration:none;display:inline;padding:3px;" onclick="selectColor('{{$color}}', '{{$shape_type}}');">&#11044;</a> --}}
	<a href="#" {{-- style="color:{{$color}};font-size:20px;text-decoration:none;display:inline;padding:3px;" --}} onclick="selectColor('{{$color}}', '{{$shape_type}}');">
		<svg height="30" width="30">
		  <circle cx="15" cy="15" r="10" stroke="black" stroke-width="1" fill="{{$color}}" />
		</svg>
	</a>
	

@endforeach