<a href="#" onclick="undo()" data-toggle="tooltip" data-placement="top" data-original-title="UNDO" style="font-size:20px;text-decoration:none;display:inline;padding:10px;">
	<b>&#8630;</b>
</a>
<a href="#" onclick="redo()" data-toggle="tooltip" data-placement="top" data-original-title="REDO" style="font-size:20px;text-decoration:none;display:inline;padding:10px;">
	<b>&#8631;</b>
</a>

<a href="#" onclick="deleteShape()" data-toggle="tooltip" data-placement="top" data-original-title="Delete Shape" style="font-size:20px;text-decoration:none;display:inline;padding:10px;">
	<b>&#128465;</b>
</a>

<a href="#" onclick="resetSketch()" data-toggle="tooltip" data-placement="top" data-original-title="Erase Board" style="font-size:20px;text-decoration:none;display:inline;padding:10px;">
	<b>&#10060;</b>
</a>
