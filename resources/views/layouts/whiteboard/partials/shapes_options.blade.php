<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="Rectangle" style="text-decoration:none;font-size:25px;padding:5px;display:inline;" onclick="selectShape('rect')">
	<b>&#11036;</b>
</a>

<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="Ellipse" style="text-decoration:none;font-size:25px;padding:5px;display:inline;" onclick="selectShape('ellipse')">
	<b>&#11093;</b>
</a>

<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="Line" style="text-decoration:none;font-size:25px;padding:5px;display:inline;" onclick="selectShape('line')">
	<b>&#8725;</b>
</a>
	
<a href="#" data-toggle="tooltip" data-placement="top"   data-original-title="Line" style="text-decoration:none;font-size:25px;padding:5px;display:inline;" onclick="selectShape('arrow')">
	<b>&#8599;</b>
</a>
<div style="display:inline;border-left: 1px solid;border-right: 1px solid;margin:15px; height:35px"> </div>

Thickness: <input type="range" min="2" max="10" value="2" class="slider" id="lineWeight" onclick="selectLineWeight(this.value);" style="display:inline;width:10%; padding:0px;">	