{{--Not in use--}}
Am I being used?
<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="TEXT" style="font-size:20px;text-decoration:none;display:inline;padding:10px;" onclick="selectShape('text')">
	<b>&#932;</b>
</a>

<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="FORMULA" style="font-size:20px;text-decoration:none;display:inline;padding-right:10px;" onclick="selectShape('formula')">
	<b>{{-- &#8750;  --}}<em style="font-family:serif">f(x)</em></b>
</a>
<div style="display:inline;">
    {!! Form::select('fontSize',array_merge(range(8,30,2), range(32,60,4)), 10, ['id' => 'fontSize', 'style' => "width:50px;display:inline", "onclick" => "updateFont('size')"]) !!}
</div>
<div class="form-group" style="display:inline;width:50px">
    {!! Form::select('font',['sans' => 'Sans', 'serif' => 'Serif'], 'sans', ['id' => 'fontStyle', 'style' => "width:50px;display:inline", "onclick" => "updateFont('style')"]) !!}
</div>	