<?php $formula_colors = ['Black', 'Gray', 'Red', 'Orange', 'DarkGreen', 'Blue', 'Purple', 'Magenta'];
	$formula_font_sizes = ["\\normalsize" => 10,
							"\\large" =>12,
							"\\Large" =>14,
							"\\LARGE" => 18,
							"\\huge" => 20,
							"\\Huge" => 24];
	
	$formula_font_styles = ['' => "Default",
							"\\fn_jvn " => "Verdana",
							"\\fn_cs " => "Comic Sans",
							"\\fn_cm " => "Modern",
							"\\fn_phv " => "Helvetica"];
?>

@foreach ($formula_colors as $formula_color)
	<a href="#" style="color:{{$formula_color}};font-size:20px;text-decoration:none;display:inline;padding:3px;" onclick="selectColor('{{$formula_color}}', '{{$shape_type}}');">&#11044;</a>
@endforeach

<div style="display:inline;">
  {!! Form::select('formulaFontSize', $formula_font_sizes, "\\huge", ['id' => 'formulaFontSize', 'style' => "width:50px;display:inline", "onclick" => "updateFont('size', 'formula');"]) !!}
</div>
<div class="form-group" style="display:inline;width:50px">
  {!! Form::select('formulaFont',$formula_font_styles, 'default', ['id' => 'formulaFontStyle', 'style' => "width:50px;display:inline", "onclick" => "updateFont('style', 'formula')"]) !!}
</div>