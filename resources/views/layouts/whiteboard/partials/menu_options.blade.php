<?php $img_size = "20px";?>
<div class="btn-toolbar">
  <div class="btn-group">
    <button class="btn" data-original-title="Info" onclick="alert('really critical info here!')">
      <img src="{{asset('images/symbols/info.svg')}}" style="height:{{$img_size}};padding:0">
    </button>
    {{-- <button id="toggle-screen" class="btn" data-original-title="Expand" onclick="toggleFullScreen()" >
      <img src="{{asset('images/symbols/expand.svg')}}" style="height:{{$img_size}};padding:0">
    </button>
    <button class="btn" data-original-title="Save" onclick="saveBoard()">
      <img src="{{asset('images/symbols/download.svg')}}" style="height:{{$img_size}};padding:0">
    </button> --}}
  </div>
  {{-- <div class="btn-group">
      <button class="btn" data-original-title="Erase Board" onclick="resetSketch();hidePalletes()">
        <img src="{{asset('images/symbols/remove.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Delete" onclick="deleteShape()">
        <img src="{{asset('images/symbols/trash.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      
      <button class="btn" data-original-title="Save" onclick="saveBoard()">
        <img src="{{asset('images/symbols/download.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Img - Ctrl+Shft+U" onclick="document.getElementById('file-upload').click()">
        <img src="{{asset('images/symbols/image.svg')}}" style="height:{{$img_size}};padding:0">
      </button> 
      <button class="btn" data-original-title="Move" onclick="changeMode('drag');hidePalletes()">
        <img src="{{asset('images/symbols/move.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Duplicate Shape" onclick="duplicateShape();">
        <img src="{{asset('images/symbols/duplicate.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Undo" onclick="undo()">
        <img src="{{asset('images/symbols/undo.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Redo" onclick="redo()">
        <img src="{{asset('images/symbols/redo.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      
  </div> --}}
  <div id="shape-buttons" class="btn-group">
      <button id="freeform-option" class="btn" data-original-title="Pencil" onclick="selectShape('pencil');drawOptions('freeform')">
        <img src="{{asset('images/symbols/pencil.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button id="freeform-option" class="btn" data-original-title="Highlighter" onclick="selectShape('highlighter');drawOptions('freeform')">
        <img src="{{asset('images/symbols/highlighter.svg')}}" style="height:{{$img_size}};padding:0">
      </button> 
      <button class="btn" data-original-title="Eraser" onclick="selectShape('eraser');drawOptions('freeform')">
        <img src="{{asset('images/symbols/eraser.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Circle" onclick="selectShape('ellipse');drawOptions('shape')">
        <img src="{{asset('images/symbols/circle.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Rectangle" onclick="selectShape('rect');drawOptions('shape')">
        <img src="{{asset('images/symbols/square.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Arrow" onclick="selectShape('arrow');drawOptions('freeform')">
      <img src="{{asset('images/symbols/arrow.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Line" onclick="selectShape('line');drawOptions('freeform')">
        <img src="{{asset('images/symbols/line.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Text" onclick="selectShape('text');drawOptions('text')">
        <img src="{{asset('images/symbols/text.svg')}}" style="height:{{$img_size}};padding:0">
      </button>
      <button class="btn" data-original-title="Formula" onclick="selectShape('formula');drawOptions('formula')">
        <img src="{{asset('images/symbols/formula.svg')}}" style="height:{{$img_size}};padding:0">
      </button> 
      <button class="btn" data-original-title="Img - Ctrl+Shft+U" onclick="document.getElementById('file-upload').click()">
        <img src="{{asset('images/symbols/image.svg')}}" style="height:{{$img_size}};padding:0">
      </button>  
  </div>
  <div id="shape-options" style="margin-left:20px;display: block">
    <div id="freeform-pallete" class="pallete" style="display: none">
      @include('layouts.whiteboard.partials.color_pallete', ['label_id' => "freeform-label", 'shape_type' => 'freeform'])
      <input type="range" min="2" max="30" value="5" class="slider" id="penWeight" style="display:inline;width:10%; padding:0px;margin: 0px 10px">
    </div>
    <div id="shape-pallete" class="pallete" style="display: none">
      @include('layouts.whiteboard.partials.color_pallete', ['label_id' => "shapes-label", 'shape_type' => 'shape'])
      <b><a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="TRANSPARENT" style="color:black;font-size:20px;text-decoration:none;display:inline;padding:17px;" onclick="selectColor('no-fill', 'shape');">&#8416;</a></b>
      <input type="range" min="2" max="10" value="2" class="slider" id="lineWeight" onclick="selectLineWeight(this.value);" style="display:inline;width:10%; padding:0px;margin: 0px 10px">
    </div>
    <div id="text-pallete" class="pallete" style="display: none">
      @include('layouts.whiteboard.partials.color_pallete', ['label_id' => "text-label", 'shape_type' => 'text'])  
      <div style="display:inline;">
          {!! Form::select('fontSize',array_merge(range(8,30,2), range(32,60,4)), 10, ['id' => 'fontSize', 'style' => "width:50px;display:inline", "onclick" => "updateFont('size', 'text')"]) !!}
      </div>
      <div class="form-group" style="display:inline;width:50px">
          {!! Form::select('fontStyle',['sans' => 'Sans', 'serif' => 'Serif'], 'sans', ['id' => 'fontStyle', 'style' => "width:50px;display:inline", "onclick" => "updateFont('style', 'text')"]) !!}
      </div>  
    </div>
    <div id="formula-pallete" class="pallete" style="display: none">
      @include('layouts.whiteboard.partials.formula_options', ['label_id' => "text-label", 'shape_type' => 'formula'])  
    </div>
  </div>
</div>