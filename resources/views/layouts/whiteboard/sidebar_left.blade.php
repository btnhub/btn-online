<div class="fixed-sidebar">
	<div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

		<a href="#" class="logo">
			<div class="img-wrap">
				<img src="{{asset('template/img/logo.png')}}" alt="Olympus">
			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OPEN MENU"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-menu-icon')}}"></use></svg>
					</a>
				</li>
				
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="LINE" style="text-decoration:none" onclick="selectShape('line')">
						<b>&#10187;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="ARROW" style="text-decoration:none" onclick="selectShape('arrow')">
						<b>&#8600;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="RECTANGLE" style="text-decoration:none" onclick="selectShape('rect')">
						<b>&#9723;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="ELLIPSE" style="text-decoration:none" onclick="selectShape('ellipse')">
						<b>&#11093;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="FREEFORM" style="text-decoration:none" onclick="selectShape('pencil')">
						<b>&#128393;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="ERASER" style="text-decoration:none" onclick="selectShape('eraser')">
						<b>&#9003;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="TEXT" style="text-decoration:none" onclick="selectShape('text')">
						<b>&#932;</b>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" data-placement="right"   data-original-title="FORMULA" style="text-decoration:none" onclick="selectShape('formula')">
						<b>&#8750;</b>
					</a>
				</li>
				<li>
					<a href="#" onclick="changeMode('drag')">
						<b>Drag</b>
					</a>
				</li>
				<li>
					<a href="#" onclick="undo()" data-toggle="tooltip" data-placement="right" data-original-title="UNDO" style="text-decoration:none">
						<b>&#8630;</b>
					</a>
				</li>
				<li>
					<a href="#" onclick="redo()" data-toggle="tooltip" data-placement="right" data-original-title="REDO" style="text-decoration:none">
						<b>&#8631;</b>
					</a>
				</li>
				<li>
					<a href="#" onclick="toggleFullScreen()" data-toggle="tooltip" data-placement="right" data-original-title="FULLSCREEN" style="text-decoration:none">
						<b>&#10530;</b>
					</a>
				</li>
				<li>
					<a href="#" onclick="" data-toggle="tooltip" data-placement="right" data-original-title="Upload File" style="text-decoration:none">
						<b onclick="document.getElementById('file-upload').click()">&#128442;</b> 
					</a>
				</li>
				
				<li>
					<a href="#" onclick="deleteShape()" data-toggle="tooltip" data-placement="right" data-original-title="Delete" style="text-decoration:none">
						<b>&#128465;</b>
					</a>
				</li>

				<li>
					<a href="#" onclick="" data-toggle="tooltip" data-placement="right" data-original-title="Erase Board" style="text-decoration:none">
						<b>&#10060;</b>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>