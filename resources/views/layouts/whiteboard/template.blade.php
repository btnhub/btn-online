<!DOCTYPE html>
<html lang="en">
<head>

	<title id="title">BuffTutor Online - Private Session</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    
    {{--P5JS--}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/p5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/addons/p5.dom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/addons/p5.sound.js"></script>

    {{--Firebase--}}
    <script src="https://www.gstatic.com/firebasejs/5.8.2/firebase.js"></script>
    <script>
	  // Initialize Firebase
	  var config = {
	    apiKey: "AIzaSyCzY4tUtjdiEk9DiaVCeJyVE_wMsHDw6LM",
	    authDomain: "btn-online-app.firebaseapp.com",
	    databaseURL: "https://btn-online-app.firebaseio.com",
	    projectId: "btn-online-app",
	    storageBucket: "btn-online-app.appspot.com",
	    messagingSenderId: "575212363028"
	  };
	  firebase.initializeApp(config);
	  
	  var firebase_key = null;

	  var ref = firebase.database().ref("classrooms/JESAIDJI2FB3");
	  ref.on('value', function(data){
	  	var room_data = data.val();
	  	
	  	firebase_key = Object.keys(room_data)[0];	
		var firebase_chat = room_data[firebase_key].chat;
	  	
	  	var chat_keys = Object.keys(firebase_chat);

	  	for (var i = 0; i < chat_keys.length; i++) {
	  		appendMessage(firebase_chat[chat_keys[i]].username, firebase_chat[chat_keys[i]].msg);
	  	}

	  	var firebase_shapes = room_data[firebase_key].shapes;
		var shapes_keys = Object.keys(firebase_shapes);

		receiveShapes(JSON.parse(firebase_shapes[shapes_keys[shapes_keys.length -1]].shapes));
	  })
	</script>

    {{-- <script src="{{asset('js/app.js')}}"></script> --}}
    {{-- @if ($demo == true)
    	<script>
            Echo.channel("{{$event_channel}}")
	            .listen('RoomUpdatedDemo', (e) =>{
	                updateRoom(JSON.parse(e.data));
	            })
	            .listen('ShapeBroadcastedDemo', (e) =>{
	                receiveShapes(JSON.parse(e.data));
	            })
	            .listen('MessageBroadcastedDemo', (e) =>{
	                receiveMessage(e.data);
	            });
    	</script>
    @else
    	<script>
    		var event_channel = "{{$event_channel}}";
            Echo.join(`room.${event_channel}`)
            	.here(users => {
            		//set shapes array and draw existing shapes
            		//shapes = .....;
            		console.log(users + "already here");
            	})
            	.joining(user => {
            		console.log(user.username + "has joined the room! Say Hi!");
            	})
            	.leaving(user => {
            		console.log(user.username + "has left the room");
            		//ajax, trigger event to update how long user was in room
            	})
            	.listen('ShapeBroadcasted', (e) =>{
	                receiveShapes(JSON.parse(e.data));
	            });
    	</script>
    @endif --}}
     
    {{--Must be before whiteboard (sketch.js)--}}
   	<script>
		{{--Load existing shapes--}}
		var prior = prior_messages = null;
		var local_user = {!!$local_user!!};

		if ({{$draw_shapes}}) {
			prior = {!!$existing_shapes_json!!};
		}
		if ({{$existing_messages}}) {
			prior_messages = JSON.stringify({!!$existing_messages_json!!});
		}
	</script>

    {{--For Ajax & Notifications--}}
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        function broadcastShape(shapes, action){
            
            ref.child(firebase_key + "/shapes").push({
				"shapes": shapes
			});

            shapes = JSON.parse(shapes);
            for (var shape of shapes){
            	shape.selected = false;
            	shape.dragging = false;
            }

            //shapes = JSON.stringify(shapes);
	
			

            var headers = null;
            
            /*if (Echo && Echo.socketId()) {
                headers = {'X-SOCKET-ID': Echo.socketId()};
            }*/


            /*$.ajax({
                type: 'POST',
                headers: headers,
                url: "{ {route('broadcast.shape')}}",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'), 
                    shapes: shapes,
                    event_channel: "{ {$event_channel}}",
                    action: action ? action : "",
                    demo: { {$demo ? 1 : 0}}
                },
                success: function(data){
                    if (data.shapes) {
                    	receiveShapes(jQuery.parseJSON(data.shapes));
                    }
                },
                error: function (err) {
			       	//console.log('error');
			        //console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
			    }
            });     */
        } 
    </script>

    {{--For Toolbar Buttons--}}
    <link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>


	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap-reboot.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/main.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/fonts.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{asset('css/mtql.css')}}">
	<script src="{{asset('js/whiteboard.js')}}"></script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
        var MQ = MathQuill.getInterface(2);
    </script>

    {{--HTML2Canvas & jsPDF--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>

    {{--Dom 2 PDF--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"></script>

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/main.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('template/css/fonts.min.css')}}">

	<!-- Main Font -->
	<script src="{{asset('template/js/webfontloader.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	{{--For Canvas--}}
	<style> body {padding: 0; margin: 0;} canvas {vertical-align: top;} </style>
	@yield('styles')
</head>

<body {{-- oncontextmenu="return false;" --}}>

	<!-- Header-BP -->
	
	@include('layouts.whiteboard.header')	
	
	<!-- ... end Header-BP -->

	<div class="header-spacer" {{-- style="background-color: lightgrey" --}}>
		<div id="options" style="position:fixed;top:57px;padding:5px;width:100%;height:6%;background-color:darkgrey">
			
			@include('layouts.whiteboard.partials.menu_options')

		</div>
	</div>

	<div id="main-content">
		<div id="formula-options" style="display:none;position:absolute;z-index:200;/*display:inline;z-index:200;position:fixed;left:700px;top:20px;*/font-size: 18px;width:450px;border:1px solid gray;padding:5px;background-color: lightgray">
			<em><small>Press Enter, Tab or Esc when done</small></em>
			<ul class="nav nav-pills">
			  <li class="active" style="padding-right:3px"><a data-toggle="pill" href="#math-symbols" class="page-link" style="padding:10px">&#177; &#8805; &oplus;</a></li>
			  <li style="padding-right:3px"><a data-toggle="pill" href="#all-greek-letters" class="page-link" style="padding:10px">&alpha; &pi; &Omega;</a></li>
			  <li style="padding-right:3px"><a data-toggle="pill" href="#arrows" class="page-link" style="padding:10px">&harr; &#8640; &dArr;</a></li>
			  <li style="padding-right:3px"><a data-toggle="pill" href="#calculus" class="page-link" style="padding:10px">&#8750; &nabla; &#8721;</a></li>
			  <li style="padding-right:3px"><a data-toggle="pill" href="#logic" class="page-link" style="padding:10px">&cup; &#8709; &isin;</a></li>
			</ul>

			<div class="tab-content">
			  <div id="math-symbols" class="tab-pane fade in active show"></div>
			  <div id="all-greek-letters" class="tab-pane fade">
			  	<div id="greek-letters"></div>
			  	<div id="capital-greek-letters"></div>
			  </div>
			  <div id="arrows" class="tab-pane fade"></div>
			  <div id="calculus" class="tab-pane fade"></div>
			  <div id="logic" class="tab-pane fade"></div>
			</div>
		</div>

		<div id="canvas-region">
			
		</div>
		<div class="side-bar" style="position:fixed;left:10px;top:12%;background-color: none;">
			<div class="side-bar-button">
				<a id="chat-button" data-original-title="Chat" onClick="toggleChat();" href="#">
					<img src="{{asset('images/symbols/chat.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Move" onclick="changeMode('select');hidePalletes();" href="#">
					<img src="{{asset('images/symbols/mouse-pointer.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Move" onclick="changeMode('drag');hidePalletes()"  href="#">
					<img src="{{asset('images/symbols/move.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Duplicate Shape" onclick="duplicateShape();" href="#">
					<img src="{{asset('images/symbols/duplicate.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Undo" onclick="undo()" href="#">
					<img src="{{asset('images/symbols/undo.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Redo" onclick="redo()" href="#">
					<img src="{{asset('images/symbols/redo.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a id="toggle-screen" data-original-title="Expand" onclick="toggleFullScreen()" href="#">
					<img src="{{asset('images/symbols/expand.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Save" onclick="saveBoard()" href="#">
					<img src="{{asset('images/symbols/download.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a data-original-title="Delete" onclick="deleteShape()" href="#">
					<img src="{{asset('images/symbols/trash.svg')}}">
				</a>
			</div>
			<div class="side-bar-button">
				<a id="toggle-screen" data-original-title="Erase Board" onclick="resetSketch();hidePalletes()" href="#">
					<img src="{{asset('images/symbols/remove.svg')}}">
				</a>
			</div>
		</div>
		<div class="canvas-arrows" id="more-left" style="bottom: 50%; left:3%">
			&#9664;&#9664;&#9664;
		</div>
		<div class="canvas-arrows" id="more-right" style="bottom: 50%;right:3%">
			&#9654;&#9654;&#9654;
		</div>
		<div class="canvas-arrows" id="more-top" style="top: 12%;right:50%">
			&#9650;&#9650;&#9650;
		</div>
		<div class="canvas-arrows" id="more-bottom" style="bottom: 3%;right:50%">
			&#9660;&#9660;&#9660;
		</div>
		
		<div class="fixed-sidebar right">
			<div style="z-index:10;position:absolute; right:0;top:0;">
				<div class="row">
					<div id="videos" class="video-fullscreen">
						<div id="subscriber">
							<div style="text-align:left;margin: 20px">
								{{-- <a href="#" id="subscriber-microphone" class="video-option" onclick="toggleMicrophone('subscriber', this.id)"><i class="fa fa-microphone" aria-hidden="true"></i></a>
								<a href="#" id="subscriber-video" class="video-option" onclick="toggleVideo('subscriber',this.id)"><i class="fas fa-video"></i></a> --}}
							</div>
						</div>
						<div id="publisher">
							{{-- <div style="position:absolute;bottom:15px;text-align:right;">
								<a href="#" id="publisher-microphone" class="video-option" onclick="toggleMicrophone(this.id)"><i class="fa fa-microphone" aria-hidden="true"></i></a>
								<a href="#" id="publisher-video" class="video-option" onclick="toggleVideo(this.id)"><i class="fas fa-video"></i></a>
							</div> --}}
							<div  id="publish-button"><center><button onclick="initializeSession()"><i class="fas fa-video"></i> Connect</button></center></div>
						</div>
					</div>	
				</div>
				<div class="row">
					<div id="video-alert" style="visibility: hidden">
						<a href="#" id="video-alert-close" onclick="closeVideoAlert()" style="padding-right:10px;">X</a>
						<span id="video-alert-msg">
						</span>
					</div>
				</div>
			</div>
			<div id="chat-window" style="display:none;">
				<div id="chat-title">
					<div style="float: left;color:#F8F8F8;font-size:20px; padding-left:20px; display:inline;vertical-align: top"><svg class="olymp-chat---messages-icon more"><use xlink:href="{{asset('template/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')}}"></use></svg></div>
					<div style="float: left;color:#F8F8F8;font-size:20px;display:inline;vertical-align: top;padding-left:10px">Chat</div>
					<div style="float: right;font-size:16px;padding:0px 10px;"><a href="#" style="text-decoration: none" onclick="toggleChat()">X</a></div>
				</div>
				<div id="chat">
				
				</div>
				<textarea id="chat-input" placeholder="Enter to send..." onkeyup="sendChatMessage(event)"></textarea>	
			</div>
		</div>

	</div>	
	

	<!-- Window-popup-CHAT for responsive min-width: 768px -->

	{{-- @include('layouts.whiteboard.chat_popup_responsive') --}}

	<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->

	
	

	{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    
    <script>
      var socket = io.connect(window.location.hostname + ":6001");
    </script> --}}

    {{--TOKBOX--}}
	@if ($activate_video)
		<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
		<script>
			var apiKey = "{{$api}}";
			var sessionId = "{{$sessionId}}";
			var token = "{{$token}}";
		</script>
		<script src="{{asset('js/va.js')}}"></script>
		
	@endif
	<link rel="stylesheet" type="text/css" href="{{asset('css/va.css')}}">	

	<!-- JS Scripts -->
{{-- 	<script>
		window.onunload = function(event) {
		    $.ajax({
                type: 'POST',
                url: "{{route('leave.room')}}",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'), 
                    user: local_user.id,
                    username: local_user.username,
                    shapes: shapes,
                    event_channel: "{{$event_channel}}",
                    demo: {{$demo ? 1 : 0}}
                },
                success: function(data){

                },
                error: function (err) {

			    }
            });     
		};	
	</script> --}}
	
	<script src="template/js/jquery.magnific-popup.js"></script>
	<script src="template/js/popper.min.js"></script>
	<script src="template/Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>