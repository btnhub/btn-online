<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layouts.whiteboard.template');
    //return view('welcome');
});
*/
Route::get('/', 'ClassroomsController@index')->name('classroom.index');

Route::get('/demo/{room?}', 'ClassroomsController@demoRoom')->name('classroom.demo');

Route::get('/room/{classroom_ref_id}', 'ClassroomsController@joinRoom')->name('join.room');

Route::post('/create-classroom', 'ClassroomsController@createRoom')->name('create.room');

Route::post('/leave-room', 'ClassroomsController@leaveRoom')->name('leave.room');

Route::delete('/delete/{classroom_ref_id}', 'ClassroomsController@deleteRoom')->name('delete.room');

//Route::post('/broadcast-shape', 'ClassroomsController@broadcastShape')->name('broadcast.shape');

//Route::post('/broadcast-message', 'ClassroomsController@broadcastMessage')->name('broadcast.message');

