<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/*Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/

Broadcast::channel('room.{classroom_ref_id}', function ($user, $classroom_ref_id) {
    if ($user->canJoinRoom($classroom_ref_id)) {
    	return ['ref_id' => $user->ref_id,
    			'username' => $user->first_name];
    }
});