<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

require_once(base_path('vendor/autoload.php')); // Loads the library
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

use OpenTok\OpenTok;
use OpenTok\Role;

//For Firebase
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

use App\Events\ShapeBroadcasted;
use App\Events\ShapeBroadcastedDemo;
use App\Events\MessageBroadcasted;
use App\Events\MessageBroadcastedDemo;
use App\Events\RoomUpdated;
use App\Events\RoomUpdatedDemo;

use Auth;
use Carbon;
use App\User;
use App\Classroom;
use App\Assignment;

class ClassroomsController extends Controller
{
    public function __construct()
    {
    	$this->sid = config('services.twilio.sid');
    	$this->token = config('services.twilio.token');
    	$this->key = config('services.twilio.key');
    	//$this->secret = config('services.twilio.secret');

    	$this->api = config('services.tokbox.api');
    	$this->secret = config('services.tokbox.secret');

        $this->expiration = 3600; //******update code to reference this value****
    }

    public function getBase64($url){
        
        $image = file_get_contents($url);
        if ($image !== false){
            //$base64 = 'data:image/svg+xml;base64,'.base64_encode($image);
            $base64 = 'data:image/png;base64,'.base64_encode($image);
            return $base64;
        }    
    }

    public function getFirebaseDB($firebase_reference){
        $firebase_json = base_path(config('firebase.json'));
        $firebase_url = config('firebase.url');
        
        $serviceAccount = ServiceAccount::fromJsonFile($firebase_json);
        
        $firebase = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri($firebase_url)
                        ->create();

        $database = $firebase->getDatabase();
        
        return $database;
    }

    public function index(){
        $user = $classrooms = $current_students = $recent_classrooms = null;

    	Auth::loginUsingId(447,true);

    	$user = User::find(auth()->user()->id);

    	if ($user->classrooms->count()) {
    		$classrooms = $user->classrooms->where('expires', '>', Carbon\Carbon::now()->addHours(4));
    	}
    	//If Tutor - compile list of active assignments/students
    	$assignments = $user->assignments/*->current()*/;
    	$assigned_students = $assignments->pluck('student_id');
    	$current_students = [];
    	
    	foreach ($assigned_students as $student_id) {
    		if ($student = User::find($student_id)) {
    			$course = $assignments->where('student_id', '=', $student_id)->first()->course;
    			$student_name = $student->full_name;
    			$current_students[$student->ref_id] = $student_name . " - " . $course;
    			asort($current_students);
    		}
    	}

    	//If Admin - get all recent classrooms
    	$recent_classrooms = Classroom::orderByDesc('id')->get();
    	
    	return view("classrooms.index", compact('user', 'classrooms','current_students', 'recent_classrooms'));
    }

   	public function demoRoom($user_id = null){

        $user = User::find($user_id ?? 10);

        $activate_video = false;
   		$demo = true;

        $draw_shapes = 0;
        $existing_shapes_json = $existing_messages_json = 0;
        $participants = [];

        $redis = Redis::connection();

   		//Get Previous Broadcasted Shapes
        $event_channel = $redis_channel = "Math-".date('Ymd');
        $chat_channel = $redis_channel."-chat";
        $participants_channel = $redis_channel . "-participants";

        $current_shape_index = intval($redis->get($event_channel . "-shapes_index"));
        $stored_shapes = $existing_shapes = [];

        if ($redis->get($redis_channel) != null && $current_shape_index != null) {
            $stored_shapes = json_decode($redis->get($redis_channel), true); //optional true = return array instead of object
            $existing_shapes = $stored_shapes[$current_shape_index];
            $existing_shapes_json = json_encode($existing_shapes);
            $draw_shapes = 1;
        }

        $existing_messages = $redis->get($chat_channel) != null ? 1 : 0;
        if ($existing_messages) {
            $existing_messages_json = json_encode($redis->get($chat_channel));    
        }
        
        if ($redis->get($participants_channel)) {
            $participants = json_decode($redis->get($participants_channel), true);
        }
        
        $action = (object)['id' => $user->ref_id, 'user' => $user->username, 'action' => 'joined_room', 'timestamp' => Carbon\Carbon::now()];

        array_push($participants, $action);
        
        $redis->setex($participants_channel, 3600, json_encode($action));        
        $redis->quit();        

        $local_user = json_encode((object)['id' => $user->ref_id, 'username' => $user->username/*, 'role' => 'tutor'*/]);

        broadcast(new RoomUpdatedDemo($event_channel, json_encode($action)))->toOthers();

   		return view('layouts.whiteboard.template', compact('activate_video', 'demo', 'event_channel', 'draw_shapes', 'existing_shapes_json', 'existing_messages', 'existing_messages_json', 'local_user'));
   	}

    public function createRoom(Request $request){
    	$tutor = auth()->user();
    	
    	if (!$tutor->canCreateRoom()) {
    		\Session::flash('error', "You are not authorized to create a room.");
			return redirect()->back();
    	}
    	
		$tutor_name = str_replace(" ", "", $tutor->full_name);
		$token_days = 2; //# days until token expires
		$token_expires = time()+($token_days*24*60*60); 

    	//Create New Classroom
    	$class_ref_id = strtoupper(str_random(12));
    	$redis_ref_id = strtoupper(str_random(8));
    	$classroom = new Classroom(['ref_id'=> $class_ref_id,
    								'creator_id' => $tutor->id,
    								'type' => "individial",
    								]);
    	$opentok = new OpenTok($this->api, $this->secret);
		
		$session_options = [];
		/*$session_options = ['archiveMode' => ArchiveMode::ALWAYS,
								'mediaMode' => MediaMode::ROUTED];*/

		$session = $opentok->createSession($session_options);    	
		$session_id = $session->getSessionId();

		//Create Redis Channel Name
		$redis_channel = "IN-" . $class_ref_id ."-".$redis_ref_id;
		//IN = individual (1-on-1) session
		
		$classroom->session_id = $session_id;
		$classroom->redis_channel = $redis_channel;
		$classroom->expires = Carbon\Carbon::now()->addDays($token_days);
		$classroom->save();

		//Create Token For Tutor
		$token_tutor = $session->generateToken([
									'expireTime' => $token_expires,
									'data' => "tutor={$tutor->id}-$tutor_name"
								]);

		//Save Token & Add Tutor to classroom_user pivot table
		$tutor->classrooms()->save($classroom, 
						   ['role' => 'tutor',
						   	'token' => $token_tutor, 
							'token_expires' => $classroom->expires]);

		//Create Token For Student(s)
		$student_count = 0;
		foreach ($request->students as $key => $student_ref_id) {	
			$student = User::where('ref_id', $student_ref_id)->first();
			if ($student) {
				$student_count++;
				$student_name = str_replace(" ", "", $student->full_name);
				$token_student = $session->generateToken([
											'expireTime' => $token_expires,
											'data' => "student{$student_count}={$student->id}-$student_name"
											]);
				//Save Token & Add Student to classroom_user pivot table 
				$student->classrooms()->save($classroom, 
							   ['role' => 'student',
								'token' => $token_student, 
								'token_expires' => $classroom->expires]);
			}
		}

		//Create Token For Admin
		$admin = User::find(62);
		$token_admin = $session->generateToken([
											'role' => ROLE::MODERATOR,
											'expireTime' => $token_expires,
											'data' => "name=admin"
											]);

		$admin->classrooms()->save($classroom, 
							   ['role' => 'student',
								'token' => $token_admin, 
								'token_expires' => $classroom->expires]);

		return redirect()->route("join.room", ['classroom_ref_id' => $classroom->ref_id]);
    }

    public function notifyStudents(Request $request){
    	//Email students link to classroom
    }

    public function joinRoom($classroom_ref_id){
        //OpenTok Session & Token
		$api = $sessionId = $token = null;
		$activate_video = 1;
		$demo = false;
        $draw_shapes = $existing_messages = 0;
        $existing_shapes_json = "";
        $current_shape_index = 1;
        $existing_messages_json = [];

		Auth::loginUsingId(116,true);
		$user = auth()->user();
		$local_user = json_encode((object)['id' => $user->ref_id, 'username' => $user->username/*, 'role' => 'tutor'*/]);

		$classroom = Classroom::where('ref_id', $classroom_ref_id)->first();
		if ($classroom && $user->canJoinRoom($classroom->ref_id)) {
			$opentok = new OpenTok($this->api, $this->secret);
			$api = $this->api;
			
			$sessionId = $classroom->session_id;
			$classroom_user = $classroom->users()->where('user_id', '=',$user->id)->first();
			$token = $classroom_user->pivot->token;
			$broadcast = $classroom_user->pivot->broadcast;
			//update pivot table "joined/entered" to now, set left/exited to null
		}
	
		else{
			\Session::flash('error', "You are not authorized to join this room");
			return redirect()->back();
		}

        //Create Firebase Entry
        $firebase_reference = "classrooms/$classroom->ref_id";
        
        $database = $this->getFirebaseDB($firebase_reference);

        $firebase_classroom = $database->getReference($firebase_reference);

        if (!$firebase_classroom->getValue()) {
            $firebase_classroom = $database->getReference($firebase_reference)
                        ->push([
                        'participants' => $classroom->users->pluck('id')->toArray(),
                        'chat' => "",
                        'shapes' => "",
                        'shapes_index' => 1,
                        'undo_count' => 0,
                        'redo_count' => 0,
                        'created_at' => Carbon\Carbon::now()->toDateTimeString()
                        ]);  
            $firebase_key = $firebase_classroom->getKey();

            $classroom->firebase_key = $firebase_key;
            $classroom->save();

            //$newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-
            //$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-
            //$newPost->getChild('title')->set('Changed post title');
            //$newPost->getValue(); // Fetches the data from the realtime database
            //$newPost->remove();
        }
        else{
            //Get Previous Chat & Shapes
            $firebase_key = $classroom->firebase_key;
            /*$chat = [
                ["created_at" => "2019-02-12 05:12:34", 
                    'username' => "Elwyn M",
                    "msg" => "Hello?! Anyone here?"], 
                ["created_at" => "2019-02-12 05:17:34",
                    'username' => "Talia S",
                    "msg" => "POLO"], 
                ["created_at" => "2019-02-12 05:15:34", 
                    'username' => "Talia S",
                    "msg" => "I mean, here!"]];

            $firebase_classroom->getChild("$firebase_key/chat")->set($chat);*/

            $existing_shapes = $firebase_classroom->getChild("$firebase_key/shapes")->getValue();
            
            if ($existing_shapes) {
                $existing_shapes_json = json_encode(end($existing_shapes)['shapes']);
            }

            $current_shapes_index = $firebase_classroom->getChild("$firebase_key/shapes_index")->getValue();    
            $existing_messages_json = json_encode($firebase_classroom->getChild("$firebase_key/chat")->getValue());  

            $draw_shapes = ($firebase_classroom->getChild("$firebase_key/shapes")->getValue() !== "") ? 1 : 0;  
            $existing_messages = ($existing_messages_json !== "") ? 1 : 0;  
        }
        

        //Get Previous Broadcasted Shapes
        $event_channel = $classroom->ref_id;

        /*$redis = Redis::connection();
        $existing_shapes = $redis->get($event_channel) ?? null; //existing shapes on redis (already broadcasted in the room)
        $draw_shapes = ($existing_shapes !== null) ? 1 : 0;
        $existing_shapes_json = json_encode($existing_shapes);
        $existing_messages_json = json_encode($existing_messages);*/
    	return view('layouts.whiteboard.template', compact('activate_video', 'demo','api', 'sessionId','token', 'event_channel', 'draw_shapes', 'existing_shapes_json', 'broadcast', 'local_user', 'existing_messages', 'existing_messages_json'));
    }

    public function joinRoomTwilio(){    	
    	//Find or Create Room
    	$sid    = $this->sid;
		$token  = $this->token;
		$client = new Client($sid, $token);
		
		$roomName = "TestSession-NewApp";
		//check if room exists
		$exists = $client->video->v1->rooms
		                          ->read(['uniqueName' => $roomName]);
		
		if (empty($exists)) {
			$client->video->v1->rooms
	                          ->create([
	                          	'uniqueName' => $roomName,
	                          	'type' => 'group',
	                          	/*'type' => 'peer-to-peer',
	                          	'enableTurn' => true,*/
	                          	'recordParticipantsOnConnect' => false
	                          	]);
		}	

		// Create an Access Token
		$identity = 'student'; //unique identifier for user

		$token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

		// Grant access to Video
		$videoGrant = new VideoGrant();
		$videoGrant->setRoom($roomName);
		$token->addGrant($videoGrant);

		$accessToken = $token->toJWT();

		//Get Previous Broadcasted Shapes
		$event_array = ['Math'/*, 'Physics'*/];
        $event_channel =$event_array[array_rand($event_array)];
        
        $redis = Redis::connection();
        $existing_shapes = $redis->get($event_channel) ?? null; //existing shapes on redis (already broadcasted in the room)
        $draw_shapes = $existing_shapes != null ? 1 : 0;
        $existing_shapes_json = json_encode($existing_shapes);


		return view('layouts.whiteboard.template', compact('accessToken', 'roomName', 'event_channel', 'draw_shapes', 'existing_shapes_json'));
    }

    public function broadcastShape(Request $request)
    {
        $return_shapes = false;

        $redis = Redis::connection();

        $redis_channel = $request->event_channel;

        if (!$request->demo) {
            $classroom = Classroom::where('ref_id', $request->event_channel)->first();
            $redis_channel = $classroom->redis_channel;
        }
        
        $current_shape_index_channel = $redis_channel . "-shapes_index";
        $redo_channel = $redis_channel . "-redo";


        if ($redis->get($redis_channel)) {
            $stored_shapes = json_decode($redis->get($redis_channel), true); //optional true = return array instead of object
        }
        else{
            //set current shape index to 1 (reset shapes archive)
            $stored_shapes[0] = json_encode([]);
            $redis->setex($redo_channel, 3600, 0); 
            $redis->setex($current_shape_index_channel, 3600, 1); 
        }

        $current_shape_index = intval($redis->get($current_shape_index_channel));
        $redo_steps = intval($redis->get($redo_channel));

        if (count(json_decode($request->shapes))) {
            switch ($request->action) {
                case 'erase-board':
                    //save shapes
                    if (!$request->$demo) {
                        //$previous_board = json_decode($classroom->board, true);
                        //$previous_board->push($request->shapes);
                        //$classroom->board = json_encode($previous_board);
                    }
                    
                    //prevent duplicates
                    //if ($request->shapes != $stored_shapes[$current_shape_index]) {
                        $current_shape_index++;
                        //$stored_shapes[$current_shape_index] = json_encode([]);
                        $stored_shapes[$current_shape_index] = $request->shapes;
                        
                        $redis->incr($current_shape_index_channel); 
                        $redis->setex($redo_channel, 3600, 0); 
                        $redis->setex($redis_channel, 3600, json_encode($stored_shapes)); //stored as stringified json, expires after 3600s
                    //}
                    break;

                case 'update':
                    //prevent duplicates
                    if ($request->shapes != $stored_shapes[$current_shape_index]) {
                        $current_shape_index++;
                        $stored_shapes[$current_shape_index] = $request->shapes;
                        
                        $redis->incr($current_shape_index_channel); 
                        $redis->setex($redo_channel, 3600, 0); 
                        $redis->setex($redis_channel, 3600, json_encode($stored_shapes)); //stored as stringified json, expires after 3600s
                    }                
                    break;

                case 'undo':
                    if ($current_shape_index > 0) {
                        $current_shape_index--;
                        $redis->decr($current_shape_index_channel); 
                        $redis->incr($redo_channel); 

                        $return_shapes = true;
                    }
                    break;

                case 'redo':
                    if($redo_steps > 0){
                        $current_shape_index++;
                        $redis->incr($current_shape_index_channel); 
                        $redis->decr($redo_channel); 

                        $return_shapes = true;
                    }
                    break;

                case 'formula':
                    //check if last shape is formula & create base64 
                    $formula_id = $base64 = null;
                    $shapes = json_decode($request->shapes);
                    $last_index = count($shapes) - 1;
                    $last_shape = array_pop($shapes);
                    
                    if ($last_shape->type == 'formula' && $last_shape->url) {
                        $last_shape->src_base64 = $this->getBase64($last_shape->url);
                        $shapes[$last_index] = $last_shape;
                        $return_shapes = true;

                        $stored_shapes[$current_shape_index] = json_encode($shapes);
                    }

                    $redis->setex($redis_channel, 3600, json_encode($stored_shapes)); //stored as stringified json, expires after 3600s  
                    break;

                default:
                    $stored_shapes[$current_shape_index] = $request->shapes;
                    $redis->setex($redis_channel, 3600, json_encode($stored_shapes)); //stored as stringified json, expires after 3600s  
                    break;   
            }
        }
        

        if ($request->demo) {
            broadcast(new ShapeBroadcastedDemo($request->event_channel, $stored_shapes[$current_shape_index]))->toOthers();    	
        }
        else{		
            broadcast(new ShapeBroadcasted("room.".$request->event_channel, $stored_shapes[$current_shape_index]))->toOthers(); 
        }

        $redis->quit();
        
        if ($return_shapes) {
            return response()->json(array('shapes'=> $stored_shapes[$current_shape_index]), 200);
        }
        else{
            return response(200);
        }
    }

    public function broadcastMessage(Request $request){

        $redis = Redis::connection();
        $redis_channel = $request->event_channel;

        $messages = [];

        if (!$request->demo) {
            $classroom = Classroom::where('ref_id', $request->event_channel)->first();
            $redis_channel = $classroom->redis_channel;
        }
        

        $chat_channel = $redis_channel . "-chat";

        if ($redis->get($chat_channel)) {
            $messages = json_decode($redis->get($chat_channel), true); //optional true = return array instead of object
        }

        $new_message = (object)['username' => $request->username, 
                                'msg' => $request->msg];

        array_push($messages, $new_message);       

        $redis->setex($chat_channel, 3600, json_encode($messages));
        

        if ($request->demo) {
            broadcast(new MessageBroadcastedDemo($request->event_channel, $new_message))->toOthers();     
        }
        else{

        }

        return response()->json(array('msg'=> json_encode($new_message)), 200);   
    }

    public function leaveRoom(Request $request){
        $shapes = $request->shapes;
        $user_ref_id = $request->user;
        $redis_channel = $request->event_channel;

        $user = User::where('ref_id', $user_ref_id)->first();

        $redis = Redis::connection();
        
        $action = (object)['id' => $user_ref_id, 'user' => $request->username, 'action' => 'left_room', 'timestamp' => Carbon\Carbon::now()];

        if ($request->demo) {
            broadcast(new RoomUpdatedDemo($request->event_channel, json_encode($action)))->toOthers();
        }
        else{
            //TO DO: Find Classroom
            //Update messages & shapes for user in classroom_user
            //$classroom = Classroom::where('redis_channel', $redis_channel)->first();
            //$classroom_user = $classroom->users()->where('user_id', '=',$user->id)->first();
            //$joined = $classroom_user->pivot->entered;
            // $original_duration = $classroom_user->pivot->duration;
            // $left = Carbon\Carbon::now();
            // $time_in_room = $left->diffInSeconds($joined);
            // $updated_duration = $original_duration + $time_in_room;
            // //update pivot table with left/exited time and duration
            broadcast(new RoomUpdated($request->event_channel, json_encode($action)))->toOthers();   
        }
        
        $participants = json_decode($redis->get($participants_channel), true);
        array_push($participants, $action);
        $redis->setex($participants_channel, 3600, json_encode($action));        
        $redis->quit();        
        
        return response(200);
    }

    public function deleteRoom(Request $request, $classroom_ref_id){
    	//$this->authorize('admin-only');
    	
    	$classroom = Classroom::where('ref_id', $classroom_ref_id)->first();
	
    	$classroom->delete();
    	
    	\Session::flash('success', "Classroom Deleted");

        return redirect()->back();
    }
}
