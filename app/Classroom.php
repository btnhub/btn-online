<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at', 'expires'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];


    /**
     * Relationships
     */
    public function users(){
        return $this->belongsToMany('App\User')
        			->withPivot('role', 'broadcast', 'entered', 'exited', 'duration', 'token', 'token_expires','deleted_at')
                    ->withTimestamps()
                    ->where('user_id', '!=', 62); //exclude admin
    }

    public function creator(){
        return $this->belongsTo('App\User', 'creator_id');
    }
}
