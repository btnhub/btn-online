<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 

    /**
     * Active Assignments: ongoing ones or ones where the end date was less than 14 days ago
     * @param  Model $query
     * @return collection       
     */
    public function scopeActive($query)
    {   
        //We continue to pay tutors for up to 14 days after end date
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 14');
    }

    /**
     * List of current & undeclined assignments 
     *     For Tutors & Students (shows up on list of assigned students/tutors)
     * @return model 
     */
    public function scopeCurrent($query)
    {   
        //Tutors will have 10 days from end date to submit charges
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 10')
                        ->where('declined', 0);
    }

    /**
     * List of ongoing assignments (declined and on-going)
     *     For Admin: for calculating next tutor payment 
     *                 - includes deleted & declined assignments 
     *                     (in case the sessions haven't been paid yet)
     * @return [type] [description]
     */
    public function scopeRecent($query)
    {   
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 30')
                        ->withTrashed();
    }

    /**
     * List of ongoing assignments (declined and on-going)
     *     For Students: to give access to evaluation forms for tutors in the past 6 months (180 days) 
     *       Condition of Date Diff > 10 days excludes assignments that are still classified as CURRENT
     * @return [type] [description]
     */
    public function scopePrevious($query)
    {   
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 180')
                        ->whereRaw('DATEDIFF(NOW(),`end_date`) > 10')
                        ->where('declined', 0);
    }

    /*public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function evaluation(){
        return $this->hasMany('App\Evaluation', 'assignment_id');
    }

    public function tutorSessions(){
        return $this->hasMany('App\TutorSession', 'assignment_id');
    }

    public function tutorContract(){
        return $this->belongsTo('App\TutorContract', 'tutor_contract_id');
    }*/
}
