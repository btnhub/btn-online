<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Classroom;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_id', 'first_name', 'last_name', 'email', 'phone', 'password',
    ];

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'last_login_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getUserNameAttribute()
    {
        return $this->first_name . " " . substr($this->last_name, 0, 1);
    }

    public function classrooms(){
        return $this->belongsToMany('App\Classroom')
                    ->withPivot('role', 'broadcast', 'entered', 'exited', 'duration', 'token', 'token_expires','deleted_at')
                    ->withTimestamps();
    }

    public function assignments()
    {   
        return $this->hasMany('App\Assignment', 'tutor_id');
    }

    public function created_rooms(){
        return $this->hasMany('App\Assignment', 'creator_id');
    }

    /**
     * Methods
     */
    
    public function canJoinRoom($classroom_ref_id){
        /*if ($this->isSuperUser()) {
            return true;
        }*/
        $classroom = Classroom::where('ref_id', $classroom_ref_id)->first();
        if ($classroom) {
            $user_ids = $classroom->users()->pluck('user_id');

            return $user_ids->contains($this->id);
        }
        
        return false;
    }

    public function canCreateRoom(){
       /*if ($this->isSuperUser()) {
            return true;
        }*/
        //Check if tutor is an active tutor
        //return $this->hasRole(['super-user', 'tutor', 'test-tutor', 'marketplace-tutor', 'music-teacher']); 
        return true;
    }
}
