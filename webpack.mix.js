let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts(['resources/assets/js/tokbox.js', 'resources/assets/js/chat.js'], 'public/js/va.js');
//mix.scripts(['resources/assets/js/btn/chat.js'], 'public/js/chat.js');
mix.scripts('resources/assets/js/btn/twilio.js', 'public/js/tw-va.js');

mix.scripts(['resources/assets/js/btn/components/*.js',], 'resources/assets/js/btn/all_components.js',);

mix.scripts(['resources/assets/js/mathquill.min.js',
		/*'resources/assets/js/tokbox.js',*/
		'resources/assets/js/btn/sketch.js',
		/*'resources/assets/js/btn/create_tab.js',*/
		'resources/assets/js/btn/toolbar.js',
		'resources/assets/js/btn/shapes.js',
		'resources/assets/js/btn/chat.js',
		'resources/assets/js/btn/all_components.js',
		], 'public/js/whiteboard.js');

mix.styles(['resources/assets/css/mathquill.css'], 'public/css/mtql.css');
mix.styles(['resources/assets/css/toolbar.css', 'resources/assets/css/tokbox.css', 'resources/assets/css/chat.css'], 'public/css/va.css');