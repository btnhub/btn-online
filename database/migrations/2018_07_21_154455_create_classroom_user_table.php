<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classroom_id');
            $table->integer('user_id');
            $table->string('role',100)->nullable();
            $table->boolean('broadcast')->default(1);
            $table->datetime('entered')->nullable();
            $table->datetime('exited')->nullable();
            $table->integer('duration')->default(0)->comment('total time in room (sec)');
            $table->string('token', 500)->nullable()->comment('tokbox token');
            $table->datetime('token_expires')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_user');
    }
}
