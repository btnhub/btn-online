<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->nullable()->unique();
            $table->string('type')->nullable();
            $table->string('session_id')->nullable()->comment('tokbox session id');
            $table->string('redis_channel')->nullable();
            $table->datetime('expires')->nullable();
            $table->smallInteger('duration')->nullable()->comment('minutes');
            $table->integer('creator_id')->nullable();
            // $table->text('board')->nullable()->comment('Final drawing');
            // $table->text('chat')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
    }
}
